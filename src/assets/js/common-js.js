import $ from 'jquery';
import AOS from 'aos';

window.jQuery = $;
window.$ = $;

console.log(this);
$(document).ready(function() {
  $('button').click(function() {
    $('button').removeClass('active');
    $(this).addClass('active');
  });
});

// AOS
$(document).ready(function() {
  AOS.init({
    easing: 'ease-in-out-sine',
    duration: 500,
    once: true,
    disable() {
      const maxWidth = 768;
      return window.innerWidth < maxWidth;
    },
  });
  if ($(window).width() < 768) {
    AOS.init({
      disable: true,
      once: false,
      disable() {
        const maxWidth = 768;
        return window.innerWidth < maxWidth;
      },
    });
  }
});

$(document).ready(function() {
  $('.drop-b').on('hide.bs.dropdown', function() {
    $('.drop-a').html(
      '<div class="d-flex align-items-center justify-content-between width-100"><span>All Categories</span><i class="fa fa-caret-right" aria-hidden="true"></i></div>'
    );
  });
  $('.drop-b').on('show.bs.dropdown', function() {
    $('.drop-a').html(
      '<div class="d-flex align-items-center justify-content-between width-100"><span>All Categories</span><i class="fa fa-caret-down" aria-hidden="true"></i></div>'
    );
  });
});

$(document).ready(function() {
  // Add minus icon for collapse element which is open by default
  $('.collapse.show').each(function() {
    $(this)
      .prev('.btn-trans')
      .find('.fa')
      .addClass('fa-caret-down')
      .removeClass('fa-caret-right');
  });

  // Toggle plus minus icon on show hide of collapse element
  $('.collapse')
    .on('show.bs.collapse', function() {
      $(this)
        .prev('.btn-trans')
        .find('.fa')
        .removeClass('fa-caret-right')
        .addClass('fa-caret-down');
    })
    .on('hide.bs.collapse', function() {
      $(this)
        .prev('.btn-trans')
        .find('.fa')
        .removeClass('fa-caret-down')
        .addClass('fa-caret-right');
    });
});
