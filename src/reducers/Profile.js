import * as actions from '../constants/ActionTypes';

const INITIALSTATE = {
  passwordForAnotherAccount: {
    value: '',
    message: '',
    isValid: false,
    isTouched: false,
  },
  currentPassword: {
    value: '',
    message: '',
    isValid: false,
    isTouched: false,
  },
  newPassword: {
    value: '',
    message: '',
    isValid: false,
    isTouched: false,
  },
  confirmPassword: {
    value: '',
    message: '',
    isValid: false,
    isTouched: false,
  },
  disabledButton: false,
  profilePicture: '',
  userProfileData: {
    name: '',
    description: '',
  },

  userSecretRecoveryCode: '',
  senderWalletAddress: {
    value: '',
    isValid: false,
    message: '',
    isTouched: false,
  },
  recieverWalletAddress: {
    value: '',
    isValid: false,
    message: '',
    isTouched: false,
  },
  storeWalletAmmount: {
    value: '',
    isValid: false,
    message: '',
    isTouched: false,
  },

  storePasswordOnSendTokken: {
    value: '',
    isValid: false,
    message: '',
    isTouched: false,
  },
};

export default (state = INITIALSTATE, action) => {
  switch (action.type) {
    case actions.STORE_PASSWORD_FOR_ANOTHER_ACCOUNT:
      console.log(typeof action.payload);
      if (typeof action.payload === 'object') {
        let response;
        let datas;
        ({ msg: response, data: datas } = action.payload);
        return {
          ...state,
          passwordForAnotherAccount: {
            value: state.passwordForAnotherAccount.value,
            message: response !== 'Success' ? datas : '',
            isValid: response === 'Success',
            isTouched: true,
          },
        };
      }
      return {
        ...state,
        passwordForAnotherAccount: {
          value: action.payload,
          message: action.payload.length === 0 ? 'Please enter password' : '',
          isValid: action.payload.length > 0,
          isTouched: true,
        },
      };
    case actions.SHOW_API_RESPONSE:
      const { msg, data } = action.payload;
      return {
        ...state,
        passwordForAnotherAccount: {
          value: state.passwordForAnotherAccount.value,
          message: data || 'Please enter password',
          isValid: msg === 'Success',
          isTouched: true,
        },
      };
    case actions.CLEAR_STORE_PASSWORD_FOR_ANOTHER_ACCOUNT:
      return {
        ...state,
        passwordForAnotherAccount: {
          value: '',
          message: '',
          isValid: false,
          isTouched: false,
        },
      };
    case actions.STORE_CURRENT_PASSWORD:
      if (typeof action.payload !== 'string') {
        let responce;
        let datas;
        ({ msg: responce, data: datas } = action.payload);

        return {
          ...state,
          currentPassword: {
            value: state.currentPassword.value,
            message: responce !== 'Success' ? datas : '',

            isValid: responce === 'Success',
            isTouched: true,
          },
        };
      }

      return {
        ...state,
        currentPassword: {
          value: action.payload,
          message: action.payload.length > 0 ? '' : 'Please enter password',
          isValid: action.payload.length > 0,
          isTouched: true,
        },
      };
    case actions.STORE_NEW_PASSWORD:
      return {
        ...state,
        newPassword: {
          value: action.payload,
          message:
            action.payload.length > 0
              ? action.payload.length >= 8
                ? ''
                : 'Please enter password of minimum 8 character long'
              : 'Please enter password',
          isValid: action.payload.length > 0 && action.payload.length >= 8,
          isTouched: true,
        },
      };
    case actions.STORE_CONFIRM_PASSWORD:
      return {
        ...state,
        confirmPassword: {
          value: action.payload,
          message:
            action.payload.length > 0
              ? action.payload === state.newPassword.value
                ? ''
                : "Password does'nt not match"
              : 'Please enter password',
          isValid: action.payload === state.newPassword.value,
          isTouched: true,
        },
      };
    case actions.DISABLED_BUTTON:
      return {
        ...state,
        disabledButton: action.payload || false,
      };
    case actions.CLEAR_STORED_PASSWORD:
      return {
        ...state,
        currentPassword: {
          value: '',
          message: '',
          isValid: false,
          isTouched: false,
        },
        newPassword: {
          value: '',
          message: '',
          isValid: false,
          isTouched: false,
        },
        confirmPassword: {
          value: '',
          message: '',
          isValid: false,
          isTouched: false,
        },
      };
    case actions.UPLOAD_PROFILE_PICTURE:
      return {
        ...state,
        profilePicture: action.payload,
      };
    case actions.GET_PROFILE_DATA_SUCCESS:
      console.log(action.payload, 'this is from saga');
      let profileImage = '';
      if (action.payload.hasOwnProperty('Image')) {
        profileImage = action.payload.Image[0].contentUrl;
      }
      return {
        ...state,
        userProfileData: {
          name: action.payload.name || '',
          description: action.payload.description || '',
        },
        profilePicture: profileImage,
      };
    case actions.RECOVER_WALLET_SUCCESS:
      return {
        ...state,
        userSecretRecoveryCode: action.payload || '',
      };
    case actions.STORE_ADDRESS_OF_SENDER:
      return {
        ...state,
        senderWalletAddress: {
          value: action.payload,
          isValid: action.payload.length > 0,
          message: action.payload.length > 0 ? '' : 'Please enter your address',
          isTouched: true,
        },
      };
    case actions.STORE_ADDRESS_OF_RECIEVER:
      return {
        ...state,
        recieverWalletAddress: {
          value: action.payload,
          isValid: action.payload.length > 0,
          message:
            action.payload.length > 0 ? '' : 'Please enter reciever address',
          isTouched: true,
        },
      };
    case actions.STORE_WALLET_AMMOUNT:
      return {
        ...state,
        storeWalletAmmount: {
          value: action.payload,
          isValid: action.payload.length > 0,
          message:
            action.payload.length > 0 ? '' : 'Please enter wallet ammount',
          isTouched: true,
        },
      };
    case actions.STORE_PASSWORD_ON_SEND_TOKKEN:
      if (typeof action.payload !== 'string') {
        let responce;
        let datas;
        ({ msg: responce, data: datas } = action.payload);

        return {
          ...state,
          storePasswordOnSendTokken: {
            value: state.storePasswordOnSendTokken.value,
            message: responce !== 'Success' ? datas.message : '',

            isValid: responce === 'Success',
            isTouched: true,
          },
        };
      }
      return {
        ...state,
        storePasswordOnSendTokken: {
          value: action.payload,
          isValid: action.payload.length > 0,
          message:
            action.payload.length > 0 ? '' : 'Please enter your password',
          isTouched: true,
        },
      };
    case actions.CLEAR_SEND_TOKKEN_DATA:
      return {
        ...state,
        senderWalletAddress: {
          value: '',
          isValid: false,
          message: '',
          isTouched: false,
        },
        recieverWalletAddress: {
          value: '',
          isValid: false,
          message: '',
          isTouched: false,
        },
        storeWalletAmmount: {
          value: '',
          isValid: false,
          message: '',
          isTouched: false,
        },
        storePasswordOnSendTokken: {
          value: '',
          isValid: false,
          message: '',
          isTouched: false,
        },
      };
    case actions.INCORRECT_RECOVERY_CODE_AT_REDIRECT:
      // let errorMsg = '';
      // if (action.payload.length > 0) {
      //   errorMsg = 'Please enter valid password';
      // }
      // if (action.payload.length == 0) {
      //   errorMsg = 'Please enter password';
      // }
      return {
        passwordForAnotherAccount: {
          message: 'Please enter valid password',
          isValid: false,
          isTouched: true,
        },
      };

    default:
      return state;
  }
};
