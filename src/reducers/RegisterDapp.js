import * as actions from '../constants/ActionTypes';

export const INITIAL_STATE = {
  dappName: '',
  dappUrl: '',
  dappEmail: '',
  dappStorage: '',
  dappCategory: '',
  dappDetails: '',
  dappToken: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actions.DAPP_NAME_CHANGED:
      return {
        ...state,
        dappName: action.payload,
      };

    case actions.DAPP_URL_CHANGED:
      return {
        ...state,
        dappUrl: action.payload,
      };

    case actions.DAPP_EMAIL_ID_CHANGED:
      return {
        ...state,
        dappEmail: action.payload,
      };

    case actions.DAPP_STORAGE_CHANGED:
      return {
        ...state,
        dappStorage: action.payload,
      };

    case actions.DAPP_CATEGORY_CHANGED:
      return {
        ...state,
        dappCategory: action.payload,
      };

    case actions.DAPP_DETAILS_CHANGED:
      return {
        ...state,
        dappDetails: action.payload,
      };

    case actions.DAPP_TOKEN_CHANGED:
      return {
        ...state,
        dappToken: action.payload,
      };

    default:
      return state;
  }
};
