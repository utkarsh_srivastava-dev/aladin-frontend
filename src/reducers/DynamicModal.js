import * as actions from '../constants/ActionTypes';

const InitialState = {
  title: '',
  body: '',
  buttonClick: '',
  cancelButton: '',
  backButton: '',
  buttonName: '',
  cancelButtonFlag: '',
  cancelButtonName: '',
  signInModalFlag: false,
  modalName: '',
  disabled: false,
  cancelIconFlag: true,
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case actions.OPEN_SIGNIN_MODAL:
      // action.payload
      console.log('TCL: action.payload', action.payload.cancelIconFlag);
      const {
        title,
        body,
        buttonClick,
        cancelButton,
        backButton,
        buttonName,
        cancelButtonFlag,
        cancelButtonName,
        cancelIconFlag,
        modalName,
        disabled,
      } = action.payload;
      return {
        title: title || '',
        body: body || '',
        backButton: backButton || '',
        buttonClick: buttonClick || '',
        cancelButton: cancelButton || '',
        buttonName: buttonName || '',
        cancelButtonFlag: cancelButtonFlag || '',
        cancelButtonName: cancelButtonName || '',
        cancelIconFlag: cancelIconFlag != false,
        signInModalFlag: true,
        modalName: modalName || '',
        disabled: disabled || false,
      };

    case actions.CLOSE_SIGNIN_MODAL:
      return {
        title: '',
        body: '',
        buttonClick: '',
        cancelButton: '',
        backButton: '',
        buttonName: '',
        cancelButtonName: '',
        cancelButtonFlag: '',
        signInModalFlag: false,
        modalName: '',
        disabled: false,
        cancelIconFlag: true,
      };

    default:
      return state;
  }
};
