import React from 'react';
import { put, takeEvery } from 'redux-saga/effects';

import axios from '../Services';
import * as actions from '../actions';
import * as actionTypes from '../constants/ActionTypes';
import { closeSuccessModal } from '../actions';

function* checkPasswordOnCreatingAnotherAccount(action) {
  try {
    const { password, userName, openSuccessModal } = action.payload;
    const { address } = JSON.parse(localStorage.getItem('userData'));
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    // const address = JSON.parse(localStorage.getItem('address')).length + 1;

    const response = yield axios
      .post('/users/createAnotherAccount', {
        mnemonicCode,
        password,
        accountNo: address.length + 1,
        network: '52.15.100.105:3000/',
        arguments: [
          userName,
          'aecc242144c615b1a2a0702e433bd74a6e0fdae931f4cde4e4c0bc13a0a8c6a101',
          'https://finalgaia.aladinnetwork.org',
        ],
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      const { accounts } = data.data;
      const userData = JSON.parse(localStorage.getItem('userData'));

      if (/ID-/g.test(accounts.idAddress)) {
        const str = accounts.idAddress.slice(3);
        userData.address.push({ address: str, username: userName });

        userData.privateKey.push({
          keys: accounts.privateKey,
          userAddress: str,
        });
      }
      localStorage.setItem('userData', JSON.stringify(userData));

      console.log(data);
      yield put(actions.disabledButton());

      yield put(
        actions.openSignInModal({
          title: 'Available names',
          body: [
            <div>
              <p>{userName}</p>
            </div>,
          ],
          buttonName: 'Register For Free',

          buttonClick: openSuccessModal,
          cancelButtonFlag: false,
        })
      );
      yield put(actions.clearStoredUserName());
      yield put(actions.clearPasswordForAnotherAccount());
      // yield put(actions.disabledButton());
      // yield put(actions.clearStoredPassword());
    } else {
      // yield put(

      //   actions.openSuccessModal({
      //     title: 'Error',
      //     message: data.data,
      //     modalStatus: 2,
      //     closeModal: onCloseSuccessModal,
      //     redirectUrl: '',
      //     showIcon: false,
      //   })
      // );
      yield put(actions.storePasswordForAnotherAccount(data));
      yield put(actions.disabledButton());
      console.log(data);
      // yield put(actions.storeCurrentPassword(data));
      // yield put(actions.disabledButton());
    }
  } catch (error) {
    console.log(error, 'This is the ');
  }
}

function* changePassword(action) {
  try {
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    console.log(mnemonicCode);
    const {
      currentPassword,
      newPassword,
      confirmPassword,
      onCloseSuccessModal,
    } = action.payload;
    const response = yield axios
      .post('/users/changePassword', {
        currentPassword,
        mnemonicCode,
        newPassword,
        newPassword2: confirmPassword,
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield put(
        actions.openSuccessModal({
          title: 'Success',
          message: data.data.message,
          modalStatus: 2,
          closeModal: onCloseSuccessModal,
          redirectUrl: '',
          showIcon: true,
        })
      );
      yield put(actions.disabledButton());
      yield put(actions.clearStoredPassword());
    } else {
      // yield put(

      //   actions.openSuccessModal({
      //     title: 'Error',
      //     message: data.data,
      //     modalStatus: 2,
      //     closeModal: onCloseSuccessModal,
      //     redirectUrl: '',
      //     showIcon: false,
      //   })
      // );
      yield put(actions.storeCurrentPassword(data));
      yield put(actions.disabledButton());
    }
  } catch (error) {
    yield console.log('hii this is my world');
  }
}

function* profileImageChanged(action) {
  yield console.log(
    'TCL: function*profileImageChanged -> action',
    action.payload
  );
  const { defaultId, privateKey } = JSON.parse(
    localStorage.getItem('userData')
  );
  const currentPrivateKey = privateKey
    .filter(item => (item.userAddress === defaultId ? item.keys : null))
    .reduce(item => item);
  // console.log(currentPrivateKey.keys, 'This is the curent');
  const response = yield axios
    .post('/users/getUploadPicURL', {
      gaiaUrl: defaultId,
      url: 'https://finalgaia.aladinnetwork.org',
      privateKey: currentPrivateKey.keys,
      ownerPrivateKey:
        'f84e66defe15b41c709614fe8c431beebe7d5d1381ae7e49cde740f320168a7501',
      image: action.payload,
    })
    .then(res => res)
    .catch(err => err);
  const data = yield response.response !== undefined
    ? response.response.data
    : response.data;
  if (response.status === 200) {
    yield console.log('TCL: function*profileImageChanged -> response', data);
    yield put(actions.uploadProfilePicture(data.data));
  } else {
    yield console.log(data);
  }
}

// GET PROFILE DATA
function* getProfileData() {
  try {
    const { defaultId } = JSON.parse(localStorage.getItem('userData'));
    console.log(defaultId);
    const response = yield axios
      .post('/users/getProfile', {
        gaiaUrl: defaultId,
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield console.log(data);
      yield put(actions.getProfileDataSuccess(data.data));
    } else {
      yield console.log(data);
    }
  } catch (error) {
    yield console.log(error);
  }
}
function* updateUserProfile(action) {
  try {
    const { defaultId, privateKey } = JSON.parse(
      localStorage.getItem('userData')
    );
    const { name, description, imageUrl } = action.payload;
    const currentPrivateKey = privateKey
      .filter(item => (item.userAddress === defaultId ? item.keys : null))
      .reduce(item => item);
    console.log(
      defaultId,
      name,
      description,
      imageUrl,
      currentPrivateKey,
      'update'
    );
    const response = yield axios
      .post('/users/editProfile', {
        name,
        description,
        imageUrl,
        gaiaHubUrl: 'https://finalgaia.aladinnetwork.org',
        privateKey: currentPrivateKey.keys,
        ownerPrivateKey:
          'f84e66defe15b41c709614fe8c431beebe7d5d1381ae7e49cde740f320168a7501',
        gaiaUrl: defaultId,
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield console.log(data);
      // yield put(actions.getProfileDataSuccess(data.data));
    } else {
      yield console.log(data);
    }
  } catch (error) {
    yield console.log('hii this is my world');
  }
}
function* recoverWallet(action) {
  try {
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    const response = yield axios
      .post('/users/recoverWallet', {
        mnemonicCode: action.payload.code,
        password: action.payload.password,
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield console.log(data);
      yield put(actions.disabledButton());
      yield put(actions.recoverWalletSuccess(data.data));
      // yield put(actions.getProfileDataSuccess(data.data));
      yield put(actions.clearPasswordForAnotherAccount());
    } else {
      yield console.log(data);
      yield put(actions.disabledButton());
      yield put(actions.storePasswordForAnotherAccount(data));
    }
  } catch (error) {
    yield console.log(error);
  }
}

function* onsendTokkenData(action) {
  try {
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    const { sender, receiver, amount, password, closeModal } = action.payload;
    const response = yield axios
      .post('/users/sendToken', {
        sender,
        receiver,
        // sender_privateKey:
        amount,
        mnemonicCode,
        password,
        accountNo: 0,
      })
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield console.log(data);
      // yield put(actions.disabledButton());
      yield put(actions.closeSignInModal());
      yield put(actions.clearSendTokkenData());
      yield put(actions.disabledButton());
      yield put(
        actions.openSuccessModal({
          title: 'Success',
          message: data.data.message,
          modalStatus: 2,
          closeModal,
          showIcon: true,
        })
      );
      // yield put(actions.recoverWalletSuccess(data.data));
      // // yield put(actions.getProfileDataSuccess(data.data));
      // yield put(actions.clearPasswordForAnotherAccount());
    } else {
      yield console.log(data);
      yield put(actions.storePasswordOnSendTokken(data));
      yield put(actions.disabledButton());
      // yield put(actions.openSuccessModal({
      //   title: 'Error',
      //   message: data.data.message,
      //   modalStatus: 2,
      //   closeModal,
      //   showIcon: false,
      // }));

      // yield put(actions.disabledButton());
      // yield put(actions.storePasswordForAnotherAccount(data));
    }
  } catch (error) {
    yield console.log(error);
  }
}
export default function* rootSaga() {
  yield takeEvery(
    actionTypes.CHECK_USER_PASSWORD,
    checkPasswordOnCreatingAnotherAccount
  );
  yield takeEvery(actionTypes.ON_CHANGE_PASSWORD, changePassword);
  yield takeEvery(actionTypes.PROFILE_IMAGE_CHANGED, profileImageChanged);
  yield takeEvery(actionTypes.GET_PROFILE_DATA, getProfileData);
  yield takeEvery(actionTypes.UPDATE_USER_PROFILE, updateUserProfile);
  yield takeEvery(actionTypes.RECOVER_WALLET, recoverWallet);
  yield takeEvery(actionTypes.SEND_TOKKEN_DATA, onsendTokkenData);
}
