import { takeEvery, put } from 'redux-saga/effects';
// import axios from 'axios';

import axios from '../Services';
import * as actions from '../actions';
import * as actionTypes from '../constants/ActionTypes';

// FUNCTION FOR CHECKING USERNAME AVALAIBILITY
function* OnCheckUserName(action) {
  yield console.log(action.payload);
  try {
    const response = yield axios
      .post(`/users/checkAvailability`, {
        bns_name: action.payload,
      })
      .then(res => res)
      .catch(err => err);

    // if (response.status === 200) {
    // const { data } = response;
    // console.log(data);
    const data =
      response.response !== undefined ? response.response.data : response.data;
    console.log(data);
    yield put(actions.formValidatorOnCheckUser(data));
    // }
  } catch (error) {
    console.log('error', error);
  }
}

// Function for Create UserId

function* onCreatUserId(action) {
  yield console.log(action);
  try {
    const { username, password, useremail } = action.payload;
    console.log(username, password, useremail);
    const response = yield axios
      .post('/users/createId', {
        username: '',
        password,
        email: useremail,
        network: '52.15.100.105:3000/',
        arguments: [
          username,
          'aecc242144c615b1a2a0702e433bd74a6e0fdae931f4cde4e4c0bc13a0a8c6a101',
          'https://finalgaia.aladinnetwork.org',
        ],
      })
      .then(res => res)
      .catch(err => err);
    const data =
      response.response !== undefined ? response.response.data : response.data;

    if (response.status === 200) {
      const { mnemonicCode, account, mnemonic } = data.data;
      const userData = {};
      const keys = [];
      if (/ID-/g.test(account.Address)) {
        const addresses = [];
        const str = account.Address.slice(3);
        addresses.push({ address: str, username });

        userData.address = addresses;
        userData.defaultId = str;
        userData.currentUser = username;
        keys.push({ keys: account.privateKey, userAddress: str });
      }
      userData.privateKey = keys;
      yield localStorage.setItem('mnemonicCode', JSON.stringify(mnemonicCode));
      yield localStorage.setItem('userData', JSON.stringify(userData));

      yield put(actions.storeUserMnemonicOnSignup(mnemonic));
      yield put(actions.disabledButton());
      yield put(actions.disabledInputs());
    }
  } catch (error) {
    console.log('error', error.message, 'This is the biggest mistakes');
  }
}

// function for get balance
function* onGetBalance(action) {
  yield console.log(action.payload);
  const { currentUser } = yield JSON.parse(localStorage.getItem('userData'));
  console.log(currentUser);
  try {
    const response = yield axios
      .post(`/users/walletQrCode`, {
        subDomain: currentUser,
      })
      .then(res => res)
      .catch(err => err);
    const data =
      response.response !== undefined ? response.response.data : response.data;

    if (response.status === 200) {
      console.log('Get Balance, Response: ', data);

      yield put(
        actions.getBalanceSuccess({
          balance: data.data.balance,
          qrcode: data.data.qrcode,
        })
      );
    } else {
      console.log(data);
    }
  } catch (error) {
    console.log('error', error);
  }
}

// function for sign in
function* signIn(action) {
  yield console.log(action.payload);
  const {
    mnemonic,
    password,
    mnemonic_code,
    mnemonic_code_password,
    onCloseSuccessModal,
  } = action.payload;

  try {
    const response = yield axios
      .post(`/users/login`, {
        mnemonic_code,
        mnemonic_code_password,
        mnemonic,
        password,
      })
      .then(res => res)
      .catch(err => err);
    // console.log("TCL: function*signIn -> response", response)
    if (response.status === 200) {
      const { account, mnemonicCode, username } = response.data.data;
      const userData = {};
      const keys = [];
      if (/ID-/g.test(account.idAddress)) {
        const addresses = [];
        const str = account.idAddress.slice(3);
        addresses.push({ address: str, username });
        userData.currentUser = username;
        userData.address = addresses;
        userData.defaultId = str;
        userData.currentUser = username;
        keys.push({ keys: account.privateKey, userAddress: str });
        userData.privateKey = keys;
      }
      yield localStorage.setItem('mnemonicCode', JSON.stringify(mnemonicCode));
      yield localStorage.setItem('userData', JSON.stringify(userData));
      yield put(
        actions.openSuccessModal({
          title: 'Success',
          message: 'Your ID has been restored.',
          modalStatus: 2,
          buttonClick: onCloseSuccessModal,
          closeModal: onCloseSuccessModal,
          redirectUrl: '/dappStore',
          showIcon: true,
        })
      );
      yield put(actions.signInRequestSuccess(response.data.data));
      yield put(actions.closeSignInModal());
      yield put(actions.recoveryKeyChanged(null));
      yield put(actions.userPasswordChanged(null));
      yield put(actions.createPasswordChanged(null));
      yield put(actions.confirmCreatePasswordChanged(null));
    } else {
      // yield put(actions.recoveryKeyChanged(null));
      // yield put(actions.recoveryKeyChanged(null))
      yield put(
        actions.openSuccessModal({
          title: 'Error',
          message: response.response.data.msg,
          modalStatus: 2,
          buttonClick: onCloseSuccessModal,
          closeModal: onCloseSuccessModal,
          showIcon: false,
        })
      );
      // yield put(actions.signInRequestError(action.payload));
    }
  } catch (error) {
    console.log('error', error);
  }
}

// function* getDapps(action) {
//   try {
//     const response = yield axios
//       .get('/dapps/getDapps')
//       .then(res => res)
//       .catch(error => console.log(error));

//     if (response.status == 200) {
//       // console.log('TCL: function*getDapps -> response', response.data.data);
//       yield put(actions.setDapps(response.data.data));
//     }
//   } catch (error) {
//     console.log('TCL: function*getDapps -> error', error);
//   }
// }

export default function* rootSaga() {
  yield takeEvery(actionTypes.CHECK_USERNAME_AVAILIBILITY, OnCheckUserName);
  yield takeEvery(actionTypes.ON_CREATE_USER_ID, onCreatUserId);
  yield takeEvery(actionTypes.GET_BALANCE, onGetBalance);
  yield takeEvery(actionTypes.SIGN_IN_REQUEST, signIn);
  // yield takeEvery(actionTypes.GET_DAPPS, getDapps);
}
