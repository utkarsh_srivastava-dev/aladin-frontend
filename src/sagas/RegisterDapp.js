import { put, takeEvery } from 'redux-saga/effects';
import axios from '../Services';
import * as actions from '../actions';
import * as actionTypes from '../constants/ActionTypes';

function* postRegisterDapp(action) {
  try {
    const {
      dappname,
      dappurl,
      emailid,
      dappstorage,
      dappcategory,
      dappdetails,
      token,
      rating,
      dapplogo,
      screenshots,
      closeSuccessModal,
    } = action.payload;

    const userData = JSON.parse(localStorage.getItem('userData'));
    const ownerId = userData.currentUser;
    const dataParams = new FormData();
    dataParams.append('dappname', dappname);
    dataParams.append('dappurl', dappurl);
    dataParams.append('emailid', emailid);
    dataParams.append('dappstorage', dappstorage);
    dataParams.append('dappcategory', dappcategory);
    dataParams.append('dappdetails', dappdetails);
    dataParams.append('token', token);
    dataParams.append('rating', rating);
    dataParams.append('ownerId', ownerId);
    for (let i = 0; i < dapplogo.length; i++) {
      dataParams.append('dapplogo', dapplogo[i]);
    }
    for (let i = 0; i < screenshots.length; i++) {
      dataParams.append('screenshots', screenshots[i]);
    }
    const response = yield axios
      .post(`dapps/registerDapp`, dataParams)
      .then(res => res)
      .catch(err => err);

    if (response.status === 200) {
      // handle response accordingly
      yield put(
        actions.openSuccessModal({
          title: 'Success',
          message: 'Your Dapp has been registered.',
          modalStatus: 2,
          buttonClick: closeSuccessModal,
          closeModal: closeSuccessModal,
          redirectUrl: '/dappStore',
          showIcon: true,
        })
      );
    } else {
      // handle response accordingly
      yield put(
        actions.openSuccessModal({
          title: 'Error',
          message: 'Sorry! Something went wrong.',
          modalStatus: 2,
          buttonClick: closeSuccessModal,
          closeModal: closeSuccessModal,
          showIcon: false,
        })
      );
    }
  } catch (error) {
    console.log('error', error);
  }
}

export default function* rootsaga() {
  yield takeEvery(actionTypes.POST_REGISTER_DAPP_REQUEST, postRegisterDapp);
}
