import { takeEvery, put } from 'redux-saga/effects';
import axios from '../Services';
import * as actions from '../actions';
import * as actionTypes from '../constants/ActionTypes';

function* getDapps(action) {
  try {
    const response = yield axios
      .get('/dapps/getDapps')
      .then(res => res)
      .catch(error => console.log(error));

    if (response.status == 200) {
      console.log('TCL: function*getDapps -> response', response.data.data);
      yield put(actions.setDapps(response.data.data));
    }
  } catch (error) {
    console.log('TCL: function*getDapps -> error', error);
  }
}

export default function* rootSaga() {
  yield takeEvery(actionTypes.GET_DAPPS, getDapps);
}
