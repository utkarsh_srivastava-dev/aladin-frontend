import React from 'react';
import { put, takeEvery } from 'redux-saga/effects';

import axios from 'axios';
import * as actions from '../actions';
import * as actionTypes from '../constants/ActionTypes';

function* getInstaData() {
  try {
    // const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));

    const response = yield axios
      .get(
        'https://api.instagram.com/v1/users/self/media/recent/?access_token=17771447131.0913e68.edb6ef044c73419bb2e180b9e67449b1'
      )
      .then(res => res)
      .catch(err => err);
    const data = yield response.response !== undefined
      ? response.response.data
      : response.data;

    if (response.status === 200) {
      yield console.log(data.data);
      yield put(actions.getInstagramDataSuccess(data.data));

      //   yield put(actions.disabledButton());
      //   yield put(actions.recoverWalletSuccess(data.data));
      //   // yield put(actions.getProfileDataSuccess(data.data));
      //   yield put(actions.clearPasswordForAnotherAccount());
    } else {
      yield console.log(data);
      //   yield put(actions.disabledButton());
      //   yield put(actions.storePasswordForAnotherAccount(data));
    }
  } catch (error) {
    yield console.log(error);
  }
}

export default function* rootSaga() {
  yield takeEvery(actionTypes.GET_INSTAGRAM_DATA, getInstaData);
}
