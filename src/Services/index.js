import axios from 'axios';

// export const baseURL = 'http://3.14.92.127:3000';

const instance = axios.create({
  baseURL: 'http://3.14.92.127:3000',
  headers: { 'Content-Type': 'application/json' },
});
export default instance;
// const user = axios.create({
//   baseURL,
//   headers: {
//     'Content-Type': 'application/x-www-form-urlencoded',
//   },
// });

// export default user;
