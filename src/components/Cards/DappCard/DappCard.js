import React from 'react';
import PropTypes from 'prop-types';
import StarRatingComponent from 'react-star-rating-component';
import StarRatings from 'react-star-ratings';
import Image from '../../Image/Image';

function DappCard(props) {
  const { id, src, title, description, rating } = props;

  return (
    <div className="col-md-3 col-lg-3 col-sm-6 col-6">
      <div className=" ipad-mar ">
        <div className="pos-relative">
          <div className="card-c show-hidea">&nbsp;</div>
          <div className="custme-height">
            <Image
              src={`http://3.14.92.127:3000${src}`}
              // src={require(`../../../assets/img/img${id}.jpg`)}
              className="imga pos-relative-a width-100 d-block mx-auto img-fluid"
            />
          </div>
        </div>
        <div className="padding-17">
          <div className="sub-title">{title}</div>
          <p className="mt-1">{description}</p>
          <div className="">
            {/* <StarRatingComponent
              // editing={false}
              value={1}
              starCount={5}
            /> */}
            <StarRatings
              rating={0}
              // starRatedColor="blue"
              // changeRating={this.changeRating}
              numberOfStars={5}
              name='rating'
              starDimension="40px"
              starSpacing="15px"
            />
            {/* <span className="nubmer-data">{rating}</span> */}
          </div>
          {/* <div> */}
          {/* <i className="fa fa-star" aria-hidden="true" />
            <i className="fa fa-star" aria-hidden="true" />
            <i className="fa fa-star" aria-hidden="true" />
            <i className="fa fa-star" aria-hidden="true" />
            <i className="fa fa-star-o" aria-hidden="true" /> */}
          {/* </div> */}
        </div>
      </div>
    </div>
  );
}

DappCard.propTypes = {
  id: PropTypes.number,
  src: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  rating: PropTypes.number,
};

export default DappCard;
