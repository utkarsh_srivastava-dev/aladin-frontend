import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, FormFeedback } from 'reactstrap';
import Image from '../Image/Image';
// import Input from '../InputControls/Input/Input';
import * as actions from '../../actions';

class NavbarWithoutLogin extends Component {
  onCloseSuccessModal = () => {
    this.props.recoveryKeyChanged(null);
    this.props.userPasswordChanged(null);
    this.props.closeSuccessModal();
  };

  onOpenSuccessModal = () => {
    const { recoveryKey, password, createPassword } = this.props;
    const { onCloseSuccessModal } = this;
    if (this.props.password.isValid) {
      this.props.signInRequest({
        mnemonic_code: recoveryKey.value,
        mnemonic_code_password: password.value,
        onCloseSuccessModal,
      });
    } else if (this.props.confirmCreatePassword.isValid) {
      this.props.signInRequest({
        mnemonic: recoveryKey.value,
        password: createPassword.value,
        onCloseSuccessModal,
      });
    }
    // else if (this.props.password.value == '') {
    //   this.props.userPasswordChanged(this.props.password.value);
    // }
  };

  emailModal = () => {
    const { email, userEmailChanged } = this.props;
    this.props.onOpenModal({
      title: 'Email Address',
      backButton: [
        <p
          className="color-red hover-icon mb-0 text-left back"
          onClick={this.emailData}
          style={{ cursor: 'pointer' }}
        >
          <i className="fa fa-long-arrow-left back" aria-hidden="true" /> Back
        </p>,
      ],
      body: [
        <div>
          <div className="pos-relative mt-3">
            <Image
              src={require('../../assets/img/msg-icon.png')}
              className="mb-3 lock-img pos-absolute"
              alt="yoo"
            />
            <Input
              type="email"
              name="email"
              placeholder="Enter Email Address"
              valid={email.isTouched ? email.isValid : false}
              invalid={email.isTouched ? !email.isValid : false}
              value={email.value}
              onChange={e => userEmailChanged(e.target.value)}
            />
            <FormFeedback
              style={{ textAlign: 'left' }}
              valid={email.isValid}
              invalid={!email.isValid}
            >
              {email.message}
            </FormFeedback>
          </div>
        </div>,
      ],
      buttonClick: this.onOpenSuccessModal,
      cancelButton: this.props.closeSignInModal,
      buttonName: 'Sign in',
      cancelButtonFlag: true,
      cancelButtonName: 'Cancel',
      modalName: 'emailModal',
    });
  };

  createPasswordModal = () => {
    const {
      createPassword,
      createPasswordChanged,
      confirmCreatePassword,
      confirmCreatePasswordChanged,
    } = this.props;

    this.props.onOpenModal({
      title: 'Create a password',
      backButton: [
        <p
          className="color-red hover-icon mb-0 text-left back"
          // onClick={this.openLoginModal}
          style={{ cursor: 'pointer' }}
        >
          <i className="fa fa-long-arrow-left back" aria-hidden="true" /> Back
        </p>,
      ],
      body: [
        <div>
          <div className="pos-relative mt-3">
            <Image
              src={require('../../assets/img/lock-img.png')}
              className="mb-3 lock-img pos-absolute"
            />
            <Input
              type="password"
              name="Password"
              placeholder="Enter Password"
              valid={createPassword.isTouched ? createPassword.isValid : null}
              invalid={
                createPassword.isTouched ? !createPassword.isValid : null
              }
              onChange={e => createPasswordChanged(e.target.value)}
            />
            <FormFeedback
              style={{ textAlign: 'left' }}
              valid={createPassword.isValid}
              invalid={!createPassword.isValid}
            >
              {createPassword.message}
            </FormFeedback>
          </div>
          <div className="pos-relative mt-3">
            <Image
              src={require('../../assets/img/lock-img.png')}
              className="mb-3 lock-img pos-absolute"
            />
            <Input
              type="password"
              name="Password"
              placeholder="Confirm Password"
              valid={
                confirmCreatePassword.isTouched
                  ? confirmCreatePassword.isValid
                  : null
              }
              invalid={
                confirmCreatePassword.isTouched
                  ? !confirmCreatePassword.isValid
                  : null
              }
              onChange={e => confirmCreatePasswordChanged(e.target.value)}
            />
            <FormFeedback
              style={{ textAlign: 'left' }}
              valid={confirmCreatePassword.isValid}
              invalid={!confirmCreatePassword.isValid}
            >
              {confirmCreatePassword.message}
            </FormFeedback>
          </div>
        </div>,
      ],
      buttonClick: () => {
        console.log(
          'TCL: Navbar -> createPasswordModal -> confirmCreatePassword.isValid',
          confirmCreatePassword.isValid
        );
        if (confirmCreatePassword.isValid) {
          this.onOpenSuccessModal();
        }
      },
      cancelButton: this.clearCloseModal,
      buttonName: 'Sign in',
      cancelButtonFlag: true,
      cancelButtonName: 'Cancel',
      modalName: 'emailData',
    });
  };

  clearCloseModal = () => {
    console.log('your are the best');
    this.props.recoveryKeyChanged(null);
    this.props.userPasswordChanged(null);
    this.props.closeSignInModal();
  };

  emailData = () => {
    const {
      password,
      userPasswordChanged,
      recoveryKey,
      closeSignInModal,
      recoveryKeyChanged,
    } = this.props;
    if (recoveryKey.isValid) {
      if (recoveryKey.type == 'code') {
        this.props.onOpenModal({
          title: 'Enter password',
          backButton: [
            <p
              className="color-red hover-icon mb-0 text-left back"
              onClick={this.openLoginModal}
              style={{ cursor: 'pointer' }}
            >
              <i className="fa fa-long-arrow-left back" aria-hidden="true" />{' '}
              Back
            </p>,
          ],
          body: [
            <div>
              <div className="pos-relative mt-3">
                <Image
                  src={require('../../assets/img/lock-img.png')}
                  className="mb-3 lock-img pos-absolute"
                  alt="yoo"
                />
                <Input
                  type="password"
                  name="Password"
                  placeholder="Enter Password"
                  valid={password.isTouched ? password.isValid : false}
                  invalid={password.isTouched ? !password.isValid : false}
                  value={password.value}
                  onChange={e => userPasswordChanged(e.target.value)}
                />
                <FormFeedback
                  style={{ textAlign: 'left' }}
                  valid={password.isValid}
                  invalid={!password.isValid}
                >
                  {password.message}
                </FormFeedback>
              </div>
            </div>,
          ],
          buttonClick: this.onOpenSuccessModal,
          cancelButton: () => {
            closeSignInModal();
            recoveryKeyChanged(null);
            userPasswordChanged(null);
          },
          buttonName: 'Sign in',
          cancelButtonFlag: true,
          cancelButtonName: 'Cancel',
          modalName: 'emailData',
        });
      } else {
        const {
          createPassword,
          createPasswordChanged,
          confirmCreatePassword,
          confirmCreatePasswordChanged,
          closeSignInModal,
          recoveryKeyChanged,
        } = this.props;

        this.props.onOpenModal({
          title: 'Create a password',
          backButton: [
            <p
              className="color-red hover-icon mb-0 text-left back"
              onClick={this.openLoginModal}
              style={{ cursor: 'pointer' }}
            >
              <i className="fa fa-long-arrow-left back" aria-hidden="true" />{' '}
              Back
            </p>,
          ],
          body: [
            <div>
              <div className="pos-relative mt-3">
                <Image
                  src={require('../../assets/img/lock-img.png')}
                  className="mb-3 lock-img pos-absolute"
                />
                <Input
                  type="password"
                  name="Password"
                  placeholder="Enter Password"
                  valid={
                    createPassword.isTouched ? createPassword.isValid : null
                  }
                  invalid={
                    createPassword.isTouched ? !createPassword.isValid : null
                  }
                  value={createPassword.value}
                  onChange={e => createPasswordChanged(e.target.value)}
                />
                <FormFeedback
                  style={{ textAlign: 'left' }}
                  valid={createPassword.isValid}
                  invalid={!createPassword.isValid}
                >
                  {createPassword.message}
                </FormFeedback>
              </div>
              <div className="pos-relative mt-3">
                <Image
                  src={require('../../assets/img/lock-img.png')}
                  className="mb-3 lock-img pos-absolute"
                />
                <Input
                  type="password"
                  name="Password"
                  placeholder="Confirm Password"
                  valid={
                    confirmCreatePassword.isTouched
                      ? confirmCreatePassword.isValid
                      : null
                  }
                  invalid={
                    confirmCreatePassword.isTouched
                      ? !confirmCreatePassword.isValid
                      : null
                  }
                  value={confirmCreatePassword.value}
                  onChange={e => confirmCreatePasswordChanged(e.target.value)}
                />
                <FormFeedback
                  style={{ textAlign: 'left' }}
                  valid={confirmCreatePassword.isValid}
                  invalid={!confirmCreatePassword.isValid}
                >
                  {confirmCreatePassword.message}
                </FormFeedback>
              </div>
            </div>,
          ],
          buttonClick: () => {
            console.log(
              'TCL: Navbar -> createPasswordModal -> confirmCreatePassword.isValid',
              confirmCreatePassword.isValid
            );
            if (confirmCreatePassword.isValid) {
              this.onOpenSuccessModal();
            }
          },
          cancelButton: () => {
            closeSignInModal();
            recoveryKeyChanged(null);
            userPasswordChanged(null);
            confirmCreatePasswordChanged(null);
            createPasswordChanged(null);
          },
          buttonName: 'Sign in',
          cancelButtonFlag: true,
          cancelButtonName: 'Cancel',
          modalName: 'emailData',
        });
      }
    } else if (this.props.recoveryKey.value == '') {
      this.props.recoveryKeyChanged(this.props.recoveryKey.value);
    }
  };

  openLoginModal = () => {
    const { recoveryKey, recoveryKeyChanged } = this.props;
    console.log('TCL: openLoginModal -> recoveryKey', recoveryKey);
    this.props.onOpenModal({
      title: 'Enter Secret Recovery Key or Magic Recovery Code',
      body: [
        <div>
          <div className=" d-block">
            <Input
              type="textarea"
              valid={recoveryKey.isTouched && recoveryKey.isValid}
              invalid={recoveryKey.isTouched && !recoveryKey.isValid}
              placeholder="Recovery Code/Key"
              onChange={e => recoveryKeyChanged(e.target.value)}
              value={recoveryKey.value}
            />
            {recoveryKey.isTouched ? (
              <FormFeedback
                style={{ textAlign: 'left' }}
                valid={recoveryKey.isValid}
                invalid={!recoveryKey.isValid}
              >
                {recoveryKey.message}
              </FormFeedback>
            ) : null}
            {console.log(recoveryKey.message)}
          </div>
        </div>,
      ],
      buttonClick: this.emailData,
      cancelButton: () => {
        this.props.closeSignInModal();
        recoveryKeyChanged(null);
      },
      buttonName: 'Sign in',
      cancelButtonFlag: true,
      cancelButtonName: 'Cancel',
      modalName: 'openLoginModal',
    });
  };

  render() {
    const { modalName, history } = this.props;
    const path = history.location.pathname;
    if (modalName != '' && this[modalName] != undefined) {
      this[modalName]();
    }
    return (
      <div id="myHeader" className="sticky-top">
        <div className="container-big">
          <nav className="navbar navbar-expand-xl navbar-light  sticky-top">
            <NavLink
              className="slideanim"
              to="/"
              title="Aladin"
              target="_blank"
            >
              <Image src={require('../../assets/img/img-logo.png')} />
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarText"
              aria-controls="navbarText"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div
              className="collapse navbar-collapse justify-content-end"
              id="navbarText"
            >
              <ul className="navbar-nav d-flex align-items-center">
                {/* <!--  <li class="nav-item ">
                           <a class="nav-link features-anchor" href="#">Try Aladin
                           </a>
                        </li> --> */}
                <li className="nav-item dropdown">
                  <NavLink
                    className={
                      path.includes('sdk') ||
                      path.includes('document') ||
                      path.includes('dappStore') ||
                      path.includes('tokenPage')
                        ? 'nav-link dropdown-toggle active'
                        : 'nav-link dropdown-toggle'
                    }
                    to="/technology"
                    id="navbardrop"
                    data-toggle="dropdown"
                  >
                    Technology
                    <i className="fa fa-chevron-down" aria-hidden="true" />
                  </NavLink>
                  <ul className="dropdown-menu">
                    <li
                      className={
                        path.includes('sdk')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/sdk"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        SDK
                      </NavLink>
                    </li>
                    <li
                      className={
                        path.includes('document')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/document"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Documentation
                      </NavLink>
                    </li>
                    <li
                      className={
                        path.includes('dappStore')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/dappStore"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Dapp store
                      </NavLink>
                    </li>
                    <li
                      className={
                        path.includes('tokenPage')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/tokenPage"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        ALA Token
                      </NavLink>
                    </li>
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  {/* <!--  <a class="nav-link" href="about-us.html" id="navbardrop">
                           About Us  
                           </a> --> */}

                  <a
                    className={
                      path.includes('team') ||
                      path.includes('aboutus') ||
                      path.includes('road-map')
                        ? 'nav-link dropdown-toggle active'
                        : 'nav-link dropdown-toggle'
                    }
                    href="#"
                    id="navbardrop"
                    data-toggle="dropdown"
                  >
                    About
                    <i
                      className="fa fa-chevron-down  d-inline"
                      aria-hidden="true"
                    />
                  </a>

                  <ul className="dropdown-menu">
                    <li
                      className={
                        path.includes('team')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/team"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Team
                      </NavLink>
                    </li>
                    <li className="dropdown-item">
                      <a href="pdf/whitepaper.pdf" target="_blank">
                        Whitepaper
                      </a>
                    </li>
                    <li className="dropdown-item">
                      <a href="pdf/Aladin-Tech-doc.pdf" target="_blank">
                        Tech-Document
                      </a>
                    </li>
                    <li className="dropdown-item">
                      <a href="pdf/Aladin-Pitchdeck.pdf" target="_blank">
                        Pitchdeck
                      </a>
                    </li>
                    <li
                      className={
                        path.includes('aboutus')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/aboutus"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        About Us
                      </NavLink>
                    </li>
                    <li
                      className={
                        path.includes('road-map')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/road-map"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Road Map
                      </NavLink>
                    </li>
                    {/* <!--   <li class="dropdown-item" ><a  href="#">Road Map</a></li> --> */}
                  </ul>
                </li>
                <li className="nav-item dropdown">
                  <a
                    className={
                      path.includes('forum') || path.includes('events')
                        ? 'nav-link dropdown-toggle active'
                        : 'nav-link dropdown-toggle'
                    }
                    href="#"
                    id="navbardrop"
                    data-toggle="dropdown"
                  >
                    Community
                    <i className="fa fa-chevron-down" aria-hidden="true" />
                  </a>
                  <ul className="dropdown-menu">
                    <li
                      className={
                        path.includes('forum')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/forum"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Forum
                      </NavLink>
                    </li>
                    <li
                      className={
                        path.includes('events')
                          ? 'dropdown-item active'
                          : 'dropdown-item'
                      }
                    >
                      <NavLink
                        to="/events"
                        // data-toggle="collapse"
                        // data-target="#navbarText"
                      >
                        Events
                      </NavLink>
                    </li>
                    <li className="dropdown-item">
                      <a href="http://aladinnetwork.org/blogs/" target="_blank">
                        Blog
                      </a>
                    </li>
                  </ul>
                </li>
                {/* <!--   <li class="nav-item ">
                           <a class="nav-link pr-0 contact-anchor" href="our-team.html">Aladin Team
                           </a>
                        </li> --> */}
                <li className="nav-item ">
                  <NavLink to="/404">
                    <a
                      className={
                        path.includes('404')
                          ? 'nav-link pr-0 contact-anchor active'
                          : 'nav-link pr-0 contact-anchor'
                      }
                    >
                      Aladin Explorer
                    </a>
                  </NavLink>
                </li>
                <li className="nav-item">
                  <div className="btn-grp">
                    <NavLink
                      to="/signup"
                      // data-toggle="collapse"
                      // data-target="#navbarText"
                    >
                      <button
                        className="btn  btn-secondary mr-3 ml-5"
                        type="button"
                      >
                        Create ID
                      </button>
                    </NavLink>
                    <button
                      className="btn btn-primary ml-3"
                      type="button"
                      data-toggle="modal"
                      data-target="#modalLoginForm"
                      onClick={this.openLoginModal}
                    >
                      Sign In
                    </button>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}
NavbarWithoutLogin.propTypes = {
  onOpenModal: PropTypes.func,
  openSuccessModal: PropTypes.func,
  closeSignInModal: PropTypes.func,
  closeSuccessModal: PropTypes.func,
  recoveryKeyChanged: PropTypes.func,
  recoveryKey: PropTypes.string,
  password: PropTypes.string,
  userPasswordChanged: PropTypes.func,
  createPassword: PropTypes.string,
  confirmCreatePassword: PropTypes.string,
  createPasswordChanged: PropTypes.func,
  confirmCreatePasswordChanged: PropTypes.func,
  email: PropTypes.string,
  userEmailChanged: PropTypes.func,
};

const mapStateToProps = ({ auth, modal, user }) => {
  const {
    recoveryKey,
    password,
    createPassword,
    confirmCreatePassword,
    email,
  } = auth;
  const { modalName } = modal;
  const { userData } = user;
  return {
    recoveryKey,
    password,
    createPassword,
    confirmCreatePassword,
    email,
    modalName,
    userData,
  };
};

const mapDispatchToProps = dispatch => ({
  onOpenModal: payload => dispatch(actions.openSignInModal(payload)),
  closeSignInModal: () => dispatch(actions.closeSignInModal()),
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
  recoveryKeyChanged: payload => dispatch(actions.recoveryKeyChanged(payload)),
  userPasswordChanged: payload =>
    dispatch(actions.userPasswordChanged(payload)),
  signInRequest: payload => dispatch(actions.signInRequest(payload)),
  createPasswordChanged: payload =>
    dispatch(actions.createPasswordChanged(payload)),
  confirmCreatePasswordChanged: payload =>
    dispatch(actions.confirmCreatePasswordChanged(payload)),
  userEmailChanged: payload => dispatch(actions.userEmailChanged(payload)),
});
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NavbarWithoutLogin)
);
