import React from 'react';
import { NavLink } from 'react-router-dom';
import Image from '../Image/Image';

function Footer() {
  return (
    <footer id="contact" className="page-section">
      <div className="container">
        <div className="row d-flex align-items-center flex-column-reverse-mob  ">
          <div className="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <div className="row ">
              <div className="col-lg-8 col-md-12 col-sm-12 col-xs-12 text">
                <NavLink to="/aboutus">About Us</NavLink>
                <span className="ml-3 mr-3">|</span>
                <NavLink to="/privacy-policy">Privacy Policy</NavLink>
                <span className="ml-3 mr-3">|</span>
                <NavLink to="/term-condition">Terms & Conditions</NavLink>
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                © 2019 Copyright Aladin
              </div>
            </div>
          </div>
          <div
            className="col-lg-4 col-md-12 col-sm-12 col-xs-12"
            id="icon-hover"
          >
            <div className="float-right float-none-mob">
              <a
                href="https://www.facebook.com/Aladinuk-100757154617610/"
                target="_blank"
                style={{ textDecoration: 'none' }}
                rel="noopener noreferrer"
              >
                <Image
                  src={require('../../assets/img/fb.png')}
                  className="mr-2"
                />
              </a>
              <a
                href="https://twitter.com/aladinnetwork"
                target="_blank"
                style={{ textDecoration: 'none' }}
              >
                <Image
                  src={require('../../assets/img/tw.png')}
                  className="mr-2"
                />
              </a>
              <a
                href="https://www.instagram.com/aladinnetwork/"
                target="_blank"
                style={{ textDecoration: 'none' }}
              >
                <Image src={require('../../assets/img/insta.png')} />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
