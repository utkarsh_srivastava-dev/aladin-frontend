import React from 'react';
import PropTypes from 'prop-types';

function VideoModal(props) {
  const { close, show, closeModal } = props;
  return (
    <div
      id="modal-container"
      className={closeModal ? (show ? 'one' : 'one out') : ''}
      onClick={close}
    >
      <div className="modal-background">
        <div className="modal" role="dialog">
          <div className="modal-dialog modal-dialog-centered  modal-lg">
            {/* Modal content */}
            <div className="modal-content" onClick={e => e.stopPropagation()}>
              <div className="modal-body">
                <video
                  style={{ height: '370', width: '100%' }}
                  controls
                  className="with-auto"
                >
                  <source src="video/Add-case.mp4" type="video/mp4" />
                  {/* <!--   <source src="mov_bbb.ogg" type="video/ogg"> --> */}
                </video>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

VideoModal.propTypes = {
  close: PropTypes.func,
  show: PropTypes.bool,
  closeModal: PropTypes.bool,
};

export default VideoModal;
