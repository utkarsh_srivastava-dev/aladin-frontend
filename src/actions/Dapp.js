import { GET_DAPPS, SET_DAPPS } from '../constants/ActionTypes';

export const getDapps = payload => ({
  type: GET_DAPPS,
  payload,
});
export const setDapps = payload => ({
  type: SET_DAPPS,
  payload,
});
