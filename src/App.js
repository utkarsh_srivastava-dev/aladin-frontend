import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import configureStore, { history } from './store';
import Layout from './Layout/Layout';
import Notfound from './modules/NotFound404/notFound404';
import asyncComponent from './util/asyncComponent';

// IMPORTED ALL CSS FILE
import './assets/fonts/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css';
import './assets/css/media.css';
import './assets/css/aos.css';
import './assets/css/dev.css';
// IMPORTED ALL JS FILE
import 'bootstrap/dist/js/bootstrap.bundle';
import './assets/js/common-js';
// import './assets/js/masonry.pkgd.min.js';
// IMPORT ALL MODULE OR COMPONENT

export const store = configureStore();
export const BLOCKSTACK_STATE_VERSION_KEY = 'BLOCKSTACK_STATE_VERSION';

const storedUser = localStorage.getItem('mnemonicCode');
const Home = asyncComponent(() => import('./modules/Information/Home/Home'));
console.log(storedUser, 'This is stored users');
const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Layout>
        <Router />
      </Layout>
    </ConnectedRouter>
  </Provider>
);

const Router = () => (
  <Switch>
    <Route
      path="/"
      exact
      component={
        //   storedUser ? (
        //     <Redirect to="/dappStore" />
        //   ) : (
        //     asyncComponent(() => import('./modules/Information/Home/Home'))

        //
        asyncComponent(() => import('./modules/Information/Home/Home'))
      }
    />
    <Route
      path="/auth"
      component={asyncComponent(() => import('./auth/index'))}
    />

    <Route
      path="/tokenPage"
      component={
        // storedUser ? (
        //   <Redirect to="/dappStore" />
        // ) : (
        //   asyncComponent(() => import('./modules/Technology/AlaToken/AlaToken'))
        // )
        asyncComponent(() => import('./modules/Technology/AlaToken/AlaToken'))
      }
    />
    <Route
      path="/dappStore"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/Information/DappStore/DappStore')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/Information/DappStore/DappStore')
        )
      }
    />
    <Route
      path="/forum"
      component={
        //   storedUser ? (
        //     <Redirect to="/dappStore" />
        //   ) : (
        //     asyncComponent(() => import('./modules/Community/Forum/Forum'))
        //   )
        //
        asyncComponent(() => import('./modules/Community/Forum/Forum'))
      }
    />
    <Route
      path="/askQuestion"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/Community/Forum/AskQuestion/AskQuestion')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/Community/Forum/AskQuestion/AskQuestion')
        )
      }
    />
    <Route
      path="/document"
      component={
        // storedUser ? (
        //   <Redirect to="/dappStore" />
        // ) : (
        //   asyncComponent(() =>
        //     import('./modules/Technology/Documentation/Documentation')
        //   )
        // )
        asyncComponent(() =>
          import('./modules/Technology/Documentation/Documentation')
        )
      }
    />
    <Route
      path="/team"
      component={
        // storedUser ? (
        //   <Redirect to="/dappStore" />
        // ) : (
        //   asyncComponent(() => import('./modules/Information/AboutUs/OurTeam'))
        // )
        asyncComponent(() => import('./modules/Information/AboutUs/OurTeam'))
      }
    />
    <Route
      path="/sdk"
      component={
        // storedUser ? (
        //   <Redirect to="dappStore" />
        // ) : (
        //   asyncComponent(() => import('./modules/Technology/SDK/SDK'))
        // )
        asyncComponent(() => import('./modules/Technology/SDK/SDK'))
      }
    />
    <Route
      path="/events"
      component={
        // storedUser ? (
        //   <Redirect to="/dappStore" />
        // ) : (
        //   asyncComponent(() => import('./modules/Community/Events/Events'))
        // )
        asyncComponent(() => import('./modules/Community/Events/Events'))
      }
    />
    <Route
      path="/signup"
      component={
        // storedUser ? (
        //   <Redirect to="/dappStore" />
        // ) : (
        //   asyncComponent(() => import('./modules/Signup/Signup'))
        // )
        // asyncComponent(() => import('./modules/Information/DappStore/RegisterDapp/RegisterDapp'))
        asyncComponent(() => import('./modules/Signup/Signup'))
      }
    />
    <Route
      path="/privacy-policy"
      component={asyncComponent(() =>
        import('./modules/Information/PrivacyPolicy/PrivacyPolicy')
      )}
    />
    <Route
      path="/term-condition"
      component={asyncComponent(() =>
        import('./modules/Information/TermCondition/TermCondition')
      )}
    />
    <Route
      path="/aboutus"
      component={asyncComponent(() =>
        import('./modules/Information/AboutUs/AboutUs')
      )}
    />
    <Route
      path="/events"
      component={asyncComponent(() =>
        import('./modules/Community/Events/Events')
      )}
    />
    <Route
      path="/wallet"
      component={
        // storedUser ? (
        //   asyncComponent(() => import('./modules/User/Wallet/Wallet'))
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() => import('./modules/User/Wallet/Wallet'))
      }
    />
    <Route
      path="/profile"
      component={
        // storedUser ? (
        //   asyncComponent(() => import('./modules/User/Profile/Profile'))
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() => import('./modules/User/Profile/Profile'))
      }
    />
    <Route
      path="/settings"
      component={
        // storedUser ? (
        //   asyncComponent(() => import('./modules/User/Settings/Settings'))
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() => import('./modules/User/Settings/Settings'))
      }
    />
    <Route
      path="/storage-provider"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/User/Settings/StorageProvider/StorageProvider')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/User/Settings/StorageProvider/StorageProvider')
        )
      }
    />
    <Route
      path="/change-password"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/User/Settings/ChangePassword/ChangePassword')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/User/Settings/ChangePassword/ChangePassword')
        )
      }
    />
    <Route
      path="/backup-and-restore"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/User/Settings/BackupRestore/BackupRestore')
        //   )

        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/User/Settings/BackupRestore/BackupRestore')
        )
      }
    />
    <Route
      path="/reset-browser"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/User/Settings/ResetBrowser/ResetBrowser')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/User/Settings/ResetBrowser/ResetBrowser')
        )
      }
    />
    <Route
      path="/api-setting"
      component={
        // storedUser ? (
        //   asyncComponent(() =>
        //     import('./modules/User/Settings/APISettings/APISetttings')
        //   )
        // ) : (
        //   <Redirect to="/" />
        // )
        asyncComponent(() =>
          import('./modules/User/Settings/APISettings/APISetttings')
        )
      }
    />
    <Route
      path="/event-detail"
      componen={asyncComponent(() =>
        import('./modules/Community/Events/EventDetail/EventDetail')
      )}
    />
    <Route
      path="/road-map"
      component={asyncComponent(() =>
        import('./modules/Information/AboutUs/RoadMap')
      )}
    />
    <Route
      path="/profile-more"
      component={asyncComponent(() =>
        import('./modules/User/Profile/ProfileMore/ProfileMore')
      )}
    />
    <Route
      path="/register-dapp"
      component={asyncComponent(() =>
        import('./modules/Information/DappStore/RegisterDapp/RegisterDapp')
      )}
    />

    {/* THIS FOR 404 PAGE NOTFOUND */}
    <Route
      path="/404"
      component={asyncComponent(() =>
        import('./modules/NotFound404/notFound404')
      )}
    />

    {/* <Redirect to="/404" component={Notfound} /> */}
  </Switch>
);

export default App;
