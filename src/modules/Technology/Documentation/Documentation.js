import React from 'react';

const documentation = () => (
  <section className="m-50 height-win" id="doucment">
    <div className="container ">
      <div className="row ">
        <div className="col-lg-4 col-md-12  col-sm-12 col-12 ">
          {/* mobile  */}
          <div className="show-mob">
            <button
              type="button"
              className="btn btn-demo btn-primary mx-auto d-block text-center"
              data-toggle="modal"
              data-target="#myModal-side"
            >
              <i className="fa fa-bars mr-2" aria-hidden="true" /> Aladin Menu
              List
            </button>
            <div
              className="modal left fade"
              id="myModal-side"
              tabIndex="-1"
              role="dialog"
              aria-labelledby="myModalLabel"
            >
              <div
                className="modal-dialog modal-dialog-centered"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-header">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <div className="bs-example " id="accordion-a">
                      <div className="accordion card" id="accordionExamplea">
                        <div className="myaccordion">
                          <div className="btn-trans" id="headingOne-a">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                              data-toggle="collapse"
                              data-target="#collapseOne-a"
                            >
                              Introduction
                              <i className="fa fa-caret-right" />
                            </button>
                          </div>
                          <div
                            id="collapseOne-a"
                            className="collapse"
                            aria-labelledby="headingOne-a"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                        <div className="myaccordion">
                          <div className=" btn-trans" id="headingTwo-b">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                              data-toggle="collapse"
                              data-target="#collapseTwo-b"
                            >
                              Building Blocks
                              <i className="fa fa-caret-right" />
                            </button>
                          </div>
                          <div
                            id="collapseTwo-b"
                            className="collapse "
                            aria-labelledby="headingTwo-b"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                        <div className="myaccordion">
                          <div className="btn-trans" id="headingThree-c">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                              data-toggle="collapse"
                              data-target="#collapseThree-c"
                            >
                              Try it! Aladin DApp
                              <i className="fa fa-caret-right" />
                            </button>
                          </div>
                          <div
                            id="collapseThree-c"
                            className="collapse"
                            aria-labelledby="headingThree-c"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                        <div className="myaccordion">
                          <div className="btn-trans" id="headingThreea-d">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                              data-toggle="collapse"
                              data-target="#collapseThreea-d"
                            >
                              Try a tutorial
                              <i className="fa fa-caret-right" />
                            </button>
                          </div>
                          <div
                            id="collapseThreea-d"
                            className="collapse"
                            aria-labelledby="headingThreea-d"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                        <div className="myaccordion">
                          <div className="btn-trans" id="headingThreeb-e">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                              data-toggle="collapse"
                              data-target="#collapseThreeb-e"
                            >
                              Work with an SDK
                              <i className="fa fa-caret-right" />
                            </button>
                          </div>
                          <div
                            id="collapseThreeb-e"
                            className="collapse"
                            aria-labelledby="headingThreeb-e"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                        <div className="myaccordion">
                          <div className="btn-trans" id="headingThreec-w">
                            <button
                              type="button"
                              className="btn width-100 d-flex align-items-center justify-content-between collapsed"
                              data-toggle="collapse"
                              data-target="#collapseThreec-w"
                              aria-label="Reference"
                            />
                            Reference
                            <i className="fa fa-caret-right" />
                          </div>
                          <div
                            id="collapseThreec-w"
                            className="collapse"
                            aria-labelledby="headingThreec-w"
                            data-parent="#accordionExamplea"
                          >
                            <div className="card-body text-left">
                              <p>Principles of Aladin applications</p>
                              <p>DApp Developer FAQs</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* modal-content  */}
              </div>
              {/* <!-- modal-dialog modal-dialog-centered--> */}
            </div>
            {/* <!-- modal --> */}
          </div>
          {/* <!-- //monbile --> */}
          <div className="bs-example show-hidea " id="accordion-a">
            <div className="accordion card" id="accordionExample">
              <div className="myaccordion">
                <div className="btn-trans" id="headingOne">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseOne"
                  >
                    Introduction
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseOne"
                  className="collapse"
                  aria-labelledby="headingOne"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
              <div className="myaccordion">
                <div className=" btn-trans" id="headingTwo">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseTwo"
                  >
                    Building Blocks
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseTwo"
                  className="collapse "
                  aria-labelledby="headingTwo"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
              <div className="myaccordion">
                <div className="btn-trans" id="headingThree">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseThree"
                  >
                    Try it! Aladin DApp
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseThree"
                  className="collapse"
                  aria-labelledby="headingThree"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
              <div className="myaccordion">
                <div className="btn-trans" id="headingThreea">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseThreea"
                  >
                    Try a tutorial
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseThreea"
                  className="collapse"
                  aria-labelledby="headingThreea"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
              <div className="myaccordion">
                <div className="btn-trans" id="headingThreeb">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseThreeb"
                  >
                    Work with an SDK
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseThreeb"
                  className="collapse"
                  aria-labelledby="headingThreeb"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
              <div className="myaccordion">
                <div className="btn-trans" id="headingThreec">
                  <button
                    type="button"
                    className="btn width-100 d-flex align-items-center justify-content-between   collapsed"
                    data-toggle="collapse"
                    data-target="#collapseThreec"
                  >
                    Reference
                    <i className="fa fa-caret-right" />
                  </button>
                </div>
                <div
                  id="collapseThreec"
                  className="collapse"
                  aria-labelledby="headingThreec"
                  data-parent="#accordionExample"
                >
                  <div className="card-body text-left">
                    <p>Principles of Aladin applications</p>
                    <p>DApp Developer FAQs</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="col-lg-8 col-md-12 col-sm-12 col-12 "
          data-aos="fade-down"
          id="document"
        >
          <div className="text-center over-flowa">
            <h4 className="pb-1">Aladin Platform</h4>
            <div className="bor-upper mx-auto" />
            <p>
              Aladin is a complete blockchain ecosystem. Aladin provides
              leverage to the Dapp Developer to establish their DApps from
              scratch, Using the SDK provided to them they would be able to make
              world-class DApps that can be used in varied use cases. The Aladin
              network allows them to deploy their DApp on the Aladin blockchain
              which uses graphene framework. It allows the users to make use of
              the decentralized storage for their Dapps which lets them enters
              into a whole new world of decentralization.
            </p>
            <p>
              Aladin network would utilize its own token ALA that would
              facilitate the user to carry out the transaction on the blockchain
              network using a different token. The main takeaway of using the
              Aladin network is zero transaction cost which allows the users to
              explore the network without worrying about the monetary aspect.
              The Aladdin allows Dapp developers to earn their money using its
              unique tokenomics model.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default documentation;
