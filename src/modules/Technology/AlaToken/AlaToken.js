import React, { Component } from 'react';
import IntroBar from '../../../components/IntroBar/IntroBar';

class AlaToken extends Component {
  render() {
    return (
      <div>
        <IntroBar>
          <h1 className="font-50">
            <span className="color-red">Aladin</span>
            <br />
            We are working to make this revolution go live soon! Stay Tuned
          </h1>

          <div className="btn-grp">
            <button className="btn btn-primary buy-botton" type="button">
              Buy
            </button>
          </div>
        </IntroBar>
        {/* <section className="bg-img img-bg-a1 aos-item" data-aos="fade-down">
          <div className="container-fluid  mt-4 pr-3 marging-bottom-0">
            <div className="row flex-column-reverse-mob">
              <div className="col-lg-6 col-sm-12 col-xs-12 d-flex align-items-center">
                <div className="pr-4 pl-4 margin-left">
                  
                </div>
              </div>
              <div
                className="col-lg-6 col-sm-12 col-xs-12  videomodal"
                id="one"
              >
                <Image
                  src={require('../../../assets/img/com-mobile-img.png')}
                  className="img-fluid img-t-10"
                />
              </div>
            </div>
          </div>
          {/* <!-- //header section image and text --> */}
        {/* </section>  */}
        {/* <!-- text section --> */}
        <section className="m-101 " id="about">
          <div className="container">
            <div className="row ">
              <div
                className="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex aos-item align-items-center"
                data-aos="fade-left"
              >
                <div>
                  <h3>
                    Aladin is a complete blockchain ecosystem. Aladin provides
                    leverage to the Dapp Developer to establish their DApps from
                    scratch, Using the SDK provided to them they would be able
                    to make world-class DApps that can be used in varied use
                    cases. The Aladin network allows them to deploy their DApp
                    on the Aladin blockchain which uses graphene framework. It
                    allows the users to make use of the decentralized storage
                    for their Dapps which lets them enters into a whole new
                    world of decentralization.
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- Resources section --> */}
        <section className=" back-color page-section" id="token-Resources">
          <div className="bg-img1 p-101 d-flex align-items-center">
            <div className="container">
              <div className="row">
                <div
                  className="col-lg-12 col-md-12 col-sm-12 col-xs-12 aos-item mx-auto"
                  data-aos="fade-down"
                >
                  <div className="pos-relative text-left">
                    <h1 className=" social-head-font">Resources</h1>
                    <div className="bor-upper mb-3" />

                    <div className="row">
                      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="card-c">
                          <p>
                            <a href="pdf/Aladin-Tech-doc.pdf" target="_blank">
                              Tech-Document
                            </a>
                          </p>

                          <p className="border-top">
                            <a href="pdf/whitepaper.pdf" target="_blank">
                              Whitepaper
                            </a>
                          </p>
                        </div>
                      </div>

                      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="card-c">
                          <p>
                            <a href="pdf/Aladin-Pitchdeck.pdf" target="_blank">
                              Pitchdeck
                            </a>
                          </p>

                          <p className="border-top">Tokenomics</p>
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="card-c">
                          <p>
                            <a href="forum1.html" target="_blank">
                              Forum
                            </a>
                          </p>

                          <p className="border-top">News</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- About Us section --> */}
        <section className="page-section" id="Aladin-One">
          <div className="bg-img1 p-101 d-flex align-items-center">
            <div className="container">
              <div className="row  aos-item" data-aos="fade-down">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto ">
                  <div>
                    <h1 className="pad-40">About Us</h1>
                    <div className="bor-upper mb-3" />
                    <p>
                      Aladin is a complete blockchain ecosystem. Aladin provides
                      leverage to the Dapp Developer to establish their DApps
                      from scratch, Using the SDK provided to them they would be
                      able to make world-class DApps that can be used in varied
                      use cases. The Aladin network allows them to deploy their
                      DApp on the Aladin blockchain which uses graphene
                      framework. It allows the users to make use of the
                      decentralized storage for their Dapps which lets them
                      enters into a whole new world of decentralization.
                    </p>
                    <p>
                      Aladin network would utilize its own token ALA that would
                      facilitate the user to carry out the transaction on the
                      blockchain network using a different token. The main
                      takeaway of using the Aladin network is zero transaction
                      cost which allows the users to explore the network without
                      worrying about the monetary aspect. The Aladdin allows
                      Dapp developers to earn their money using its unique
                      tokenomics model.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- Key Dates section --> */}
        <section
          className=" back-color page-section aos-item"
          id="features"
          data-aos="fade-down"
        >
          <div className="bg-img1 p-101 d-flex align-items-center">
            <div className="container">
              <h1 className=" social-head-font">Key Dates</h1>
              <div className="bor-upper mb-2" />
              <div className="kyc-top-bar  clearfix">
                <ul>
                  <li className="current">
                    <div className="box">
                      <div className="icon ic-1" />
                    </div>
                    <div>
                      <p>Aladin Blockchain Goes Live</p>
                    </div>
                  </li>

                  <li className="current">
                    <div className="box">
                      <div className="icon ic-1" />
                    </div>
                    <div>
                      <p>Agreessive Global Marketing</p>
                    </div>
                  </li>
                  <li className="current">
                    <div className="box">
                      <div className="icon ic-2" />
                    </div>
                    <div>
                      <p>Seminar's for Global Developer</p>
                    </div>
                  </li>
                  <li>
                    <div className="box">
                      <div className="icon ic-3" />
                    </div>
                    <div>
                      <p>
                        Aladin will Develop and Launch Dapps on Aladin to Drive
                        the Ecosytem
                      </p>
                    </div>
                  </li>
                  <li>
                    <div className="box">
                      <div className="icon ic-3" />
                    </div>
                    <div>
                      <p>DaPP Marketplace Launches</p>
                    </div>
                  </li>
                  <li>
                    <div className="box">
                      <div className="icon ic-3" />
                    </div>
                    <div>
                      <p>Searchable Blockain Browser Goes Live</p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        {/* END */}
      </div>
    );
  }
}

export default AlaToken;
