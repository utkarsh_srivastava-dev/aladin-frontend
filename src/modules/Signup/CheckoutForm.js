import React from 'react';
import {
  injectStripe,
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
} from 'react-stripe-elements';
import { prop } from 'styled-tools';

function CheckoutForm(props) {
  let Timer;
  const createOptions = () => ({
    style: {
      base: {
        fontSize: '16px',
        color: '#424770',
        fontFamily: 'Open Sans, sans-serif',
        letterSpacing: '0.025em',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#c23d4b',
      },
    },
  });

  const submitHandler = async () => {
    const { token } = await props.stripe.createToken({ name: 'Name' });
    console.log(token);
    // const response = await fetch('3.14.92.127:3000/stripe/getPayment', {
    //   method: 'POST',
    //   headers: { 'Content-Type': 'application/json' },
    //   body: {
    //     stripeToken: token.id,
    //     finalChargeAmount: 100,
    //     subDomain: 'utkarsha.a.com',
    //   },
    // });
    // console.log(response);
  };
  return (
    <div>
      <CardElement {...createOptions()} />
      <input type="text" />
      <button className="btn btn-success btn-sm" onClick={submitHandler}>
        Pay
      </button>
      {/* <CardNumberElement />
      <CardExpiryElement />
      <CardCVCElement /> */}
    </div>
  );
}

export default injectStripe(CheckoutForm);
