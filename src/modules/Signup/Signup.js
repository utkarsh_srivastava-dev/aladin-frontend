import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, FormFeedback } from 'reactstrap';
import * as blockstack from 'blockstack';
import { UserSession, AppConfig } from 'blockstack';
import {
  CardElement,
  injectStripe,
  StripeProvider,
  Elements,
} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
// import Input from '../../components/InputControls/Input/Input';
import Image from '../../components/Image/Image';
import Button from '../../components/InputControls/Button/Button';
import SecretKey from './SecretKey/SecretKey';
import * as actions from '../../actions';

const appConfig = new AppConfig();
const userSession = new UserSession({ appConfig });

const regEx = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
let TIMEOUT;
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSecretKey: false,
      accept: false,
      clickedButton: false,
      isTouched: false,
      stripe: false,
      Ala: false,
    };
    this.inputs = React.createRef();
  }

  showSecretKeySection = () => {
    this.setState({ showSecretKey: true });
  };

  handleSignIn(e) {
    e.preventDefault();
    userSession.redirectToSignIn();
  }

  // Function for showing modal
  onShowModal = () => {
    const { openSuccessModal, closeSuccessModal, onClearUserName } = this.props;
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    console.log(mnemonicCode);
    if (mnemonicCode) {
      onClearUserName();
      openSuccessModal({
        title: 'Thank You',
        message: `We have successfully created your Aladin ID`,
        modalStatus: 2,
        showIcon: false,
        buttonClick: closeSuccessModal,

        redirectUrl: '/dappStore',
      });
    }
  };

  // Check userName availability
  checkUserNameAvailibility = e => {
    e.preventDefault();
    const {
      onCheckUserNameAvailaibility,
      userName,
      formValidatorOnCheckUser,
      onStoreUserName,
    } = this.props;
    if (userName.value.length === 0) {
      formValidatorOnCheckUser({});
    }
    if (
      userName.value.length > 0 &&
      /(?<=\s|^)[^.\s]+\.[^.\s]+\.[^.\s]/gm.test(userName.value)
    ) {
      console.log(userName.value);

      onCheckUserNameAvailaibility(userName.value);
    }
  };

  // Function For Submit form
  OnSubmitUserForm = e => {
    e.preventDefault();
    console.log('hii');
    const {
      userName,
      userEmail,
      userPassword,
      userConfirmPassword,
      openSuccessModal,
      closeSuccessModal,
      onStoreUserEmail,
      onStoreUserPassword,
      onStoreUserName,
      onStoreUserConfirmPassWord,
      onCreatUserId,
      formValidatorOnCheckUser,
      onDisabledButton,
    } = this.props;
    let error = false;
    console.log(userName, userPassword, userEmail);
    if (userName.value.length === 0) {
      console.log('please enter username');
      onStoreUserName(userName.value);
      error = true;
    } else if (!userName.isValid) {
      console.log('please enter data');
    } else if (userEmail.value.length === 0) {
      onStoreUserEmail(userEmail.value);
      console.log('please enter username');
      error = true;
    } else if (!regEx.test(userEmail.value)) {
      console.log('please enter username');
      onStoreUserEmail(userEmail.value);

      error = true;
    } else if (userPassword.value.length === 0) {
      console.log('please enter username');
      onStoreUserPassword(userPassword.value);
      error = true;
    } else if (userPassword.value.length < 8) {
      onStoreUserPassword(userPassword.value);
      console.log('please enter username');
      error = true;
    } else if (userConfirmPassword.value.length === 0) {
      onStoreUserConfirmPassWord(userConfirmPassword.value);
      console.log('please enter username');
      error = true;
    } else if (userConfirmPassword.value !== userPassword.value) {
      onStoreUserConfirmPassWord(userConfirmPassword.value);
      console.log('please enter username');

      error = true;
    } else if (!error) {
      const userData = {
        username: userName.value.trim(),
        password: userPassword.value.trim(),
        useremail: userEmail.value.trim(),
      };
      onDisabledButton(true);
      onCreatUserId(userData);
      // onStoreUserConfirmPassWord('');
      // onStoreUserEmail('');
      // onStoreUserPassword('');
      // onStoreUserName('');
    }
  };

  onSelectAla = () => {
    this.setState({ Ala: true, stripe: false });
  };

  onSelectStripe = () => {
    this.setState({ Ala: false, stripe: true });
  };

  render() {
    const { showSecrSetKey, accept, clickedButton, isTouched } = this.state;
    const {
      onStoreUserName,
      userName,
      userEmail,
      userPassword,
      userConfirmPassword,
      onStoreUserEmail,
      onStoreUserPassword,
      onStoreUserConfirmPassWord,
      userMnemonic,
      disabledInputs,
      disabledButton,
    } = this.props;

    return (
      <div>
        <section className="" id="about">
          <div className="container">
            <div className="row d-flex align-items-center justify-content-center m-50">
              <div
                className="col-lg-5 col-md-12 col-sm-12 col-xs-12 d-flex aos-item mx-auto align-items-center justify-content-center"
                data-aos="fade-down"
              >
                <div className="text-center">
                  <h1 className="pb-1">Lets Register!!</h1>
                  <div className="bor-upper mx-auto" />
                  <p className="text-left">
                    Register yourself and enjoy the blockchain world of Aladin
                  </p>
                  <form onSubmit={this.OnSubmitUserForm} className="user-form">
                    <div>
                      <div className="pos-relative">
                        <Image
                          src={require('../../assets/img/icon-a.png')}
                          className="mb-3 pos-absolute"
                        />
                        <Input
                          type="text"
                          valid={
                            userName.isTouched &&
                            userName.isValid &&
                            !disabledInputs
                          }
                          invalid={userName.isTouched && !userName.isValid}
                          placeholder="Username.a.com"
                          onBlur={this.checkUserNameAvailibility}
                          onChange={e => onStoreUserName(e.target.value)}
                          value={userName.value}
                          ref={this.inputs}
                          disabled={disabledInputs}
                          required
                        />
                        {userName.isTouched ? (
                          <FormFeedback
                            style={{ textAlign: 'left' }}
                            valid={userName.isValid}
                            invalid={!userName.isValid}
                          >
                            {userName.message}
                          </FormFeedback>
                        ) : null}
                        {/* <Input
                          type="text"
                          name="user"
                          className="form-control"
                          placeholder="Enter User Name"
                          data-error="Valid User Name is required."
                          onBlur={this.checkUserNameAvailibility}
                          onChange={e => OnStoreUserName(e.target.value)}
                          value={userName} 
                        /> */}
                      </div>
                      <div className="pos-relative mt-3">
                        <Image
                          src={require('../../assets/img/msg-icon.png')}
                          className="mb-3 pos-absolute"
                        />
                        <Input
                          type="email"
                          name="email"
                          placeholder="Email ID"
                          valid={
                            userEmail.isTouched
                              ? userEmail.isValid && !disabledInputs
                              : null
                          }
                          invalid={
                            userEmail.isTouched ? !userEmail.isValid : null
                          }
                          onChange={e => onStoreUserEmail(e.target.value)}
                          value={userEmail.value}
                          disabled={disabledInputs}
                          required
                        />
                        <FormFeedback
                          style={{ textAlign: 'left' }}
                          valid={userEmail.isValid}
                          invalid={!userEmail.isValid}
                        >
                          {userEmail.message}
                        </FormFeedback>

                        {/* <Input
                          type="email"
                          name="email"
                          className="form-control"
                          placeholder="Email ID"
                          data-error="Valid Password is required."
                          onChange={e => onStoreUserEmail(e.target.value)}
                          value={userEmail}
                        /> */}
                      </div>
                      <div className="pos-relative mt-3 text-left d-flex">
                        <h5 className="text-secondary font-weight-normal mr-4">
                          Payment Info
                        </h5>
                        <div>
                          <div className="form-check form-check-inline mr-3">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="inlineRadioOptions"
                              id="inlineRadio1"
                              value="option1"
                              onChange={this.onSelectStripe}
                              style={{ cursor: 'pointer' }}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="inlineRadio1"
                            >
                              stripe
                            </label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="inlineRadioOptions"
                              id="inlineRadio2"
                              value="option2"
                              style={{ cursor: 'pointer' }}
                              onChange={this.onSelectAla}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="inlineRadio2"
                            >
                              ALA coin
                            </label>
                          </div>
                        </div>
                      </div>
                      {/* PAYMEnt */}
                      {this.state.Ala ? (
                        <div className="pos-relative mt-3">
                          <Image
                            src={require('../../assets/img/lock-img.png')}
                            className="mb-3 lock-img pos-absolute"
                          />
                          <Input
                            type="password"
                            name="Password"
                            placeholder="Enter Password"
                            valid={
                              userPassword.isTouched
                                ? userPassword.isValid && !disabledInputs
                                : null
                            }
                            invalid={
                              userPassword.isTouched
                                ? !userPassword.isValid
                                : null
                            }
                            onChange={e => onStoreUserPassword(e.target.value)}
                            value={userPassword.value}
                            disabled={disabledInputs}
                            required
                          />
                          <FormFeedback
                            style={{ textAlign: 'left' }}
                            valid={userPassword.isValid}
                            invalid={!userPassword.isValid}
                          >
                            {userPassword.message}
                          </FormFeedback>
                        </div>
                      ) : null}
                      {this.state.stripe ? (
                        <StripeProvider apiKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
                          <Elements>
                            <CheckoutForm userName={userName} />
                          </Elements>
                        </StripeProvider>
                      ) : null}

                      <div className="pos-relative mt-3">
                        <Image
                          src={require('../../assets/img/lock-img.png')}
                          className="mb-3 lock-img pos-absolute"
                        />
                        <Input
                          type="password"
                          name="Password"
                          placeholder="Enter Password"
                          valid={
                            userPassword.isTouched
                              ? userPassword.isValid && !disabledInputs
                              : null
                          }
                          invalid={
                            userPassword.isTouched
                              ? !userPassword.isValid
                              : null
                          }
                          onChange={e => onStoreUserPassword(e.target.value)}
                          value={userPassword.value}
                          disabled={disabledInputs}
                          required
                        />
                        <FormFeedback
                          style={{ textAlign: 'left' }}
                          valid={userPassword.isValid}
                          invalid={!userPassword.isValid}
                        >
                          {userPassword.message}
                        </FormFeedback>
                        {/* <Input
                          type="password"
                          name="Password"
                          className="form-control"
                          placeholder="Enter Password"
                          data-error="Valid Password is required."
                          onChange={e => onStoreUserPassword(e.target.value)}
                          value={userPassword}
                        /> */}
                      </div>
                      <div className="pos-relative mt-3">
                        <Image
                          src={require('../../assets/img/lock-img.png')}
                          className="mb-3 lock-img pos-absolute"
                        />
                        <Input
                          type="password"
                          name="Password"
                          placeholder="Confirm Password"
                          valid={
                            userConfirmPassword.isTouched
                              ? userConfirmPassword.isValid && !disabledInputs
                              : null
                          }
                          invalid={
                            userConfirmPassword.isTouched
                              ? !userConfirmPassword.isValid
                              : null
                          }
                          onChange={e =>
                            onStoreUserConfirmPassWord(e.target.value)
                          }
                          value={userConfirmPassword.value}
                          disabled={disabledInputs}
                          required
                        />
                        <FormFeedback
                          style={{ textAlign: 'left' }}
                          valid={userConfirmPassword.isValid}
                          invalid={!userConfirmPassword.isValid}
                        >
                          {userConfirmPassword.message}
                        </FormFeedback>
                        {/* <Input
                          type="password"
                          name="Password"
                          className="form-control"
                          placeholder="Confirm Password"
                          data-error="Valid Password is required."
                          onChange={e =>
                            onStoreUserConfirmPassWord(e.target.value)
                          }
                          value={userConfirmPassword}
                        /> */}
                      </div>

                      <label
                        className="container-a text-left"
                        htmlFor="checkbox"
                        onChange={() =>
                          this.setState({
                            accept: !accept,
                            isTouched: true,
                            clickedButton: !clickedButton,
                          })
                        }
                      >
                        <p
                          className="pad-10 color-black-a"
                          style={{
                            cursor: disabledInputs ? 'not-allowed' : 'pointer',
                          }}
                        >
                          <a
                            style={{
                              pointerEvents: disabledInputs ? 'none' : '',

                              textDecoration: disabledInputs ? 'none' : '',
                            }}
                          >
                            I accept the user license agreement
                          </a>
                        </p>
                        <Input
                          type="checkbox"
                          id="checkbox"
                          className="password-chekbox"
                          checked={accept}
                          disabled={disabledInputs}
                          style={{
                            cursor: disabledInputs ? 'not-allowed' : 'pointer',
                          }}
                        />
                        <span
                          className="checkmark left-align"
                          style={{
                            cursor: disabledInputs ? 'not-allowed' : 'pointer',
                          }}
                        />
                      </label>
                      <FormFeedback
                        style={
                          isTouched && !accept
                            ? { textAlign: 'left', display: 'block' }
                            : {}
                        }
                        // valid={accept}
                        invalid={!accept}
                      >
                        Please accept license agreement
                      </FormFeedback>
                      {/* {accept ? (
                        "Please accept license agreement"
                      ) : (
                        <span
                          style={{
                            textAlign: 'left',
                            color: 'red',
                            display: 'inline',
                          }}
                        >
                          Please accept license agreement
                        </span>
                      )} */}
                      <Button
                        className="btn btn-primary mt-3 request_demo_send width-100"
                        type="submit"
                        disabled={
                          !(
                            accept &&
                            userName.isValid &&
                            userEmail.isValid &&
                            userPassword.isValid &&
                            userConfirmPassword.isValid &&
                            userName.userNameApiResponse
                          ) ||
                          disabledInputs ||
                          disabledButton
                        }
                      >
                        {disabledButton ? 'Loading...' : 'Set Go'}
                      </Button>
                      {/* for sign-in with block stack button uncomment below code. */}
                      {/* <Button
                        className="btn btn-primary btn-lg"
                        id="signin-button"
                        onClick={this.handleSignIn}>
                        Sign In with Blockstack
                      </Button> */}
                    </div>
                  </form>
                  {userMnemonic ? (
                    <SecretKey
                      open={this.onShowModal}
                      mnemonic={userMnemonic}
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
Signup.propTypes = {
  onOpenModal: PropTypes.func,
  closeSignInModal: PropTypes.func,
  openSuccessModal: PropTypes.func,
  closeSuccessModal: PropTypes.func,
  onCheckUserNameAvailaibility: PropTypes.func,
  onStoreUserName: PropTypes.func,
  userName: PropTypes.string,
  userEmail: PropTypes.string,
  userPassword: PropTypes.string,
  userConfirmPassword: PropTypes.string,
  onStoreUserEmail: PropTypes.func,
  onStoreUserPassword: PropTypes.func,
  onStoreUserConfirmPassWord: PropTypes.func,
  onCreatUserId: PropTypes.func,
  formValidatorOnCheckUser: PropTypes.func,
  userMnemonic: PropTypes.string,
  disabledInputs: PropTypes.bool,
};

const mapStateToProps = ({ auth, profile }) => {
  const {
    userName,
    userEmail,
    userPassword,
    userConfirmPassword,
    formValidator,
    userMnemonic,
    disabledInputs,
  } = auth;
  const { disabledButton } = profile;

  return {
    userName,
    userEmail,
    userPassword,
    userConfirmPassword,
    formValidator,
    userMnemonic,
    disabledInputs,
    disabledButton,
  };
};

const mapDispatchToProps = dispatch => ({
  onOpenModal: payload => dispatch(actions.openSignInModal(payload)),
  closeSignInModal: () => dispatch(actions.closeSignInModal()),
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
  onCheckUserNameAvailaibility: payload =>
    dispatch(actions.checkUserNameAvailibility(payload)),
  onStoreUserName: payload =>
    dispatch(actions.storeUserNameOnCheckAvailibility(payload)),
  onStoreUserEmail: payload => dispatch(actions.storeUserEmail(payload)),
  onStoreUserPassword: payload => dispatch(actions.storeUserPassword(payload)),
  onStoreUserConfirmPassWord: payload =>
    dispatch(actions.storeUserConfirmPassword(payload)),
  onCreatUserId: payload => dispatch(actions.onCreatUserId(payload)),
  formValidatorOnCheckUser: payload =>
    dispatch(actions.formValidatorOnCheckUser(payload)),
  onClearUserName: () => dispatch(actions.clearStoredUserName()),
  onDisabledInput: payload => dispatch(actions.disabledButton(payload)),
  onDisabledButton: payload => dispatch(actions.disabledButton(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
