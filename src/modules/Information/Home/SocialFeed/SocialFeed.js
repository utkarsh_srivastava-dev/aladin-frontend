import React from 'react';
// import { FacebookProvider, Like } from 'react-facebook';
// import { TwitterTimelineEmbed } from 'react-twitter-embed';

function SocialFeed() {
  return (
    <div>
      <section
        className=" back-color page-section aos-item"
        id="features"
        data-aos="fade-down"
      >
        <div className="bg-img1 p-101 d-flex align-items-center">
          <div className="container">
            <h1 className="pad-40 social-head-font">Social Feeds</h1>
            <div className="bor-upper mb-2" />
            <div className="row">
              <div className="col-lg-12">
                {/* <!-- Nav pills --> */}
                <ul className="nav nav-pills" role="tablist">
                  {/* <!-- <li class="nav-item">
                           <a class="nav-link active" data-toggle="pill" href="#menu">All
                           </a>
                           </li> --> */}
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      data-toggle="pill"
                      href="#menu1"
                    >
                      Facebook
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu2">
                      Twitter
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu3">
                      Youtube
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu4">
                      Instagram
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="pill" href="#menu5">
                      Github
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  {/* <!-- all --> */}
                  {/* <!--   <div id="menu" class=" tab-pane active">
                           <div class="row">
                             <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                               <div class="card-c">
                                 <div class="pad-30-social ">
                                   <div class="d-flex mb-3 flex-wrap social-post align-items-center">
                                     <img src="img/img-icon1.png" class="img-circle"/>
                                     <div class="ml-3 ">
                                       <h4>Aladin
                                       </h4>
                                       <p class="sub-title">@Group Name
                                       </p>
                                     </div>
                                   </div>
                                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dignissim, augue in elementum suscipit, est ante porta sapien, ut sodales ex odio id est. Fusce non blandit ipsum. Morbi ut vehicula nibh. Sed tincidunt justo eu nisi varius sagittis. Ut eget tempus erat, et 
                                   </p>
                                 </div>
                                 <div class="fb-bg  d-flex align-items-center">
                                   <i class="fa fa-facebook" aria-hidden="true"></i><span>Posted 02 hrs ago</span>
                                 </div>
                               </div>
                             </div>
                               <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                               <div class="card-c">
                                 <div class="pad-30-social ">
                                   <div class="d-flex mb-3 flex-wrap social-post align-items-center">
                                     <img src="img/img-icon1.png" class="img-circle"/>
                                     <div class="ml-3 ">
                                       <h4>Aladin
                                       </h4>
                                       <p class="sub-title">@Group Name
                                       </p>
                                     </div>
                                   </div>
                                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dignissim, augue in elementum suscipit, est ante porta sapien, ut sodales ex odio id est. Fusce non blandit ipsum. Morbi ut vehicula nibh. Sed tincidunt justo eu nisi varius sagittis. Ut eget tempus erat, et 
                                   </p>
                                 </div>
                                 <div class="tw-bg d-flex align-items-center">
                                  <i class="fa fa-twitter" aria-hidden="true"></i><span>Posted 02 hrs ago</span>
                                 </div>
                               </div>
                             </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                               <div class="card-c">
                                 <div class="pad-30-social ">
                                   <div class="d-flex mb-3 flex-wrap social-post align-items-center">
                                     <img src="img/img-icon1.png" class="img-circle"/>
                                     <div class="ml-3 ">
                                       <h4>Aladin
                                       </h4>
                                       <p class="sub-title">@Group Name
                                       </p>
                                     </div>
                                   </div>
                                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dignissim, augue in elementum suscipit, est ante porta sapien, ut sodales ex odio id est. Fusce non blandit ipsum. Morbi ut vehicula nibh. Sed tincidunt justo eu nisi varius sagittis. Ut eget tempus erat, et 
                                   </p>
                                 </div>
                                    <div class="fb-bg  d-flex align-items-center">
                                   <i class="fa fa-facebook" aria-hidden="true"></i><span>Posted 02 hrs ago</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                           </div> --> */}
                  {/* <!--//all --> */}
                  {/* <!-- Facebook --> */}
                  <div id="menu1" className=" tab-pane active">
                    <div className="row">
                      <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div className="card-c">
                          {/* <FacebookProvider appId="123456789">
                            <Like
                              href="http://www.facebook.com"
                              colorScheme="dark"
                              showFaces
                              share
                            />
                          </FacebookProvider> */}
                          <iframe
                            src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAladinuk-100757154617610%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=245199429337790"
                            width="100%"
                            height="500"
                            style={{ border: 'none', overflow: 'hidden' }}
                            scrolling="no"
                            frameBorder="0"
                            allowTransparency="true"
                            allow="encrypted-media"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <!-- //Facebook --> */}
                  {/* <!-- twitter --> */}
                  <div id="menu2" className=" tab-pane fade">
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="card-c height-500">
                          <a
                            className="twitter-timeline"
                            href="https://twitter.com/aladinnetwork?ref_src=twsrc%5Etfw"
                          >
                            Tweets by aladinnetwork
                          </a>{' '}
                          <script
                            async
                            src="https://platform.twitter.com/widgets.js"
                            charset="utf-8"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <!-- //twitter --> */}
                  {/* <!-- youtube --> */}
                  <div id="menu3" className="mt-4 text-center  tab-pane fade">
                    <div className="row mt-4">
                      <div className="col-lg-4">
                        <div>
                          <iframe
                            width="100%"
                            height="315"
                            src="https://www.youtube.com/embed/WdiiaeOGgQk"
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                          />
                        </div>
                      </div>
                      <div className="col-lg-4">
                        <div>
                          <iframe
                            width="100%"
                            height="315"
                            src="https://www.youtube.com/embed/WdiiaeOGgQk"
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                          />
                        </div>
                      </div>
                      <div className="col-lg-4">
                        <div>
                          <iframe
                            width="100%"
                            height="315"
                            src="https://www.youtube.com/embed/WdiiaeOGgQk"
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default SocialFeed;
