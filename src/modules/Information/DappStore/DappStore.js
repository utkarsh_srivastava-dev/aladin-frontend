import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Input, FormFeedback } from 'reactstrap';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Image from '../../../components/Image/Image.jsx';
import * as actions from '../../../actions';
import DappCard from '../../../components/Cards/DappCard/DappCard';
import { validateAndCleanRecoveryInput } from '../../../util/encryption-utils';

class DappStore extends Component {
  state = {
    copied: false,
    dappData: [
      {
        id: 1,
        title: 'Graphite',
        description: '  Encrypted, shareable, decentr -alized personal data.',
        src: 'img1.jpg',
      },
      {
        id: 2,
        title: 'Zinc',
        description: '  Work based identity & reputation system.',
        src: 'img1.jpg',
      },
      {
        id: 3,
        title: 'Recall',
        description: 'Your end-to-end encrypted and open-source photo vault a…',
        src: 'img1.jpg',
      },
      {
        id: 4,
        title: 'Graphite',
        description: '  Encrypted, shareable, decentr -alized personal data.',
        src: 'img1.jpg',
      },
      {
        id: 5,
        title: 'Graphite',
        description: '  Encrypted, shareable, decentr -alized personal data.',
        src: 'img1.jpg',
      },
    ],
  };

  componentWillMount() {
    const { dapps, getDapps } = this.props;
    getDapps();
  }

  componentDidMount() {
    const url =
      'http://localhost:5000/dappStore/id=do0Dsllgq2KuWT86OS3dJn2yw9aFf/vw14IPyOXE246Mftv4YeGtuE/uhubW/x4dHVGqkbCZ/4b4mUYAho7+q/6sty5ChbQg6qo+lq9N2I0=';
    console.log(window.location.href);
    if (url == window.location.href) {
      this.getRecoveryKey();
    }

    // const getRecoveryKey = () => {
    //   this.props.openSignInModal({
    //     title: 'Save your Recovery key',
    //     // backButton: [
    //     //   <p
    //     //     className="color-red hover-icon mb-0 text-left back"
    //     //     onClick={this.openLoginModal}
    //     //     style={{ cursor: 'pointer' }}
    //     //   >
    //     //     <i className="fa fa-long-arrow-left back" aria-hidden="true" /> Back
    //     //   </p>,
    //     // ],
    //     body: [
    //       <div>
    //         <div className="pos-relative mt-3">
    //           {/* <Image
    //             src={require('../../../assets/img/lock-img.png')}
    //             className="mb-3 lock-img pos-absolute"
    //             alt="yoo"
    //           />
    //           <Input
    //             type="password"
    //             name="Password"
    //             placeholder="Enter Password"
    //             valid={password.isTouched ? password.isValid : false}
    //             invalid={password.isTouched ? !password.isValid : false}
    //             // value={password.value}
    //             onChange={e => userPasswordChanged(e.target.value)}
    //           />
    //           <FormFeedback
    //             style={{ textAlign: 'left' }}
    //             valid={password.isValid}
    //             invalid={!password.isValid}
    //           >
    //             {password.message}
    //           </FormFeedback> */}
    //         </div>
    //       </div>,
    //     ],
    //     buttonClick: this.showRecoveryKey,
    //     cancelButton: () => {
    //       // closeSignInModal();
    //       // userPasswordChanged(null);
    //     },
    //     buttonName: 'Next',
    //     cancelButtonFlag: false,
    //     cancelButtonName: 'Cancel',
    //     modalName: 'emailData',
    //     cancelIconFlag: false,
    //   });
    // };

    // const url = 'http://localhost:3000/dappStore/id=1234';
    // console.log(
    //   'TCL: Layout -> componentDidMount -> window.location.href',
    //   window.location.href
    // );
    // if (url === window.location.href) {
    //   console.log('working url');

    //   const { password, userPasswordChanged, closeSignInModal } = this.props;

    //   this.props.openSignInModal({
    //     title: 'Enter password',
    //     // backButton: [
    //     //   <p
    //     //     className="color-red hover-icon mb-0 text-left back"
    //     //     onClick={this.openLoginModal}
    //     //     style={{ cursor: 'pointer' }}
    //     //   >
    //     //     <i className="fa fa-long-arrow-left back" aria-hidden="true" /> Back
    //     //   </p>,
    //     // ],
    //     body: [
    //       <div>
    //         <div className="pos-relative mt-3">
    //           <Image
    //             src={require('../../../assets/img/lock-img.png')}
    //             className="mb-3 lock-img pos-absolute"
    //             alt="yoo"
    //           />
    //           <Input
    //             type="password"
    //             name="Password"
    //             placeholder="Enter Password"
    //             valid={password.isTouched ? password.isValid : false}
    //             invalid={password.isTouched ? !password.isValid : false}
    //             // value={password.value}
    //             onChange={e => userPasswordChanged(e.target.value)}
    //           />
    //           <FormFeedback
    //             style={{ textAlign: 'left' }}
    //             valid={password.isValid}
    //             invalid={!password.isValid}
    //           >
    //             {password.message}
    //           </FormFeedback>
    //         </div>
    //       </div>,
    //     ],
    //     buttonClick: getRecoveryKey,
    //     cancelButton: () => {
    //       // closeSignInModal();
    //       userPasswordChanged(null);
    //     },
    //     buttonName: 'Next',
    //     cancelButtonFlag: false,
    //     cancelButtonName: 'Cancel',
    //     modalName: 'emailData',
    //     cancelIconFlag: false,
    //   });
    // }

    window.scrollTo(0, 0);
  }

  // getRecoveryKey = () => {
  //   const {
  //     passwordForAnotherAccount,
  //     disabledButton,
  //     userSecretRecoveryCode,
  //     storePasswordForAnotherAccount,
  //     modalName,
  //   } = this.props;
  //   // console.log(passwordForAnotherAccount.isValid, 'dnkdnindkvndivndk');
  //   this.props.openSignInModal({
  //     title: 'Enter Password',

  //     body: [
  //       <div>
  //         <div className="pos-relative mt-3">
  //           <Image
  //             src={require('../../../assets/img/lock-img.png')}
  //             className="mb-3 lock-img pos-absolute"
  //             alt="yoo"
  //           />
  //           <Input
  //             type="password"
  //             name="password"
  //             placeholder="Enter your password"
  //             // value={passwordForAnotherAccount.value}
  //             valid={
  //               passwordForAnotherAccount.isTouched
  //                 ? passwordForAnotherAccount.isValid
  //                 : false
  //             }
  //             invalid={
  //               passwordForAnotherAccount.isTouched
  //                 ? !passwordForAnotherAccount.isValid
  //                 : false
  //             }
  //             // value={email.value}
  //             onChange={e => storePasswordForAnotherAccount(e.target.value)}
  //           />
  //           <FormFeedback
  //             style={{ textAlign: 'left' }}
  //             valid={passwordForAnotherAccount.isValid}
  //             invalid={!passwordForAnotherAccount.isValid}
  //           >
  //             {passwordForAnotherAccount.message}
  //           </FormFeedback>
  //         </div>
  //       </div>,
  //     ],
  //     buttonClick: this.onSubmitPassword,

  //     buttonName: 'Submit',
  //     cancelButtonFlag: false,
  //     cancelButtonName: 'Cancel',
  //     cancelIconFlag: false,
  //     modalName: 'getRecoveryKey',
  //   });
  // };

  onCopyHandler = () => {
    this.setState({ copied: true });

    setTimeout(() => {
      this.setState({ copied: false });
    }, 3000);
  };

  hexConvertor = dataBuffer => {
    const buffer = new Buffer.from(dataBuffer, 'base64');
    const base = buffer.toString('hex');
    return base;
  };

  onSubmitPassword = () => {
    const { copied } = this.state;
    const {
      passwordForAnotherAccount,
      onDisabledButton,
      recoverWallet,
      userSecretRecoveryCode,
    } = this.props;

    if (passwordForAnotherAccount.value.length > 0) {
      const index = window.location.pathname.indexOf('=');
      console.log('TCL: DappStore -> onSubmitPassword -> index', index);
      const temp = window.location.pathname.slice(index + 1);
      console.log('TCL: DappStore -> onSubmitPassword -> temp', temp);
      const valid = validateAndCleanRecoveryInput(temp);
      const mCode = this.hexConvertor(temp);
      console.log('TCL: DappStore -> onSubmitPassword -> mCode', mCode);

      onDisabledButton(true);
      if (valid.isValid) {
        recoverWallet({
          code: mCode,
          password: passwordForAnotherAccount.value,
        });
        if (userSecretRecoveryCode) {
          console.log('recovery code is there');
          this.props.openSignInModal({
            title: 'Recovery Key',

            body: [
              <div className="password-a mt-3 ">
                <div className=" d-block">
                  <p>12 - Digit Recover Key</p>
                  <textarea className="form-control" disabled="disabled">
                    {userSecretRecoveryCode}
                  </textarea>
                </div>
                <div className="pos-relative mt-3">
                  <CopyToClipboard
                    text={userSecretRecoveryCode}
                    onCopy={this.onCopyHandler}
                  >
                    <a style={{ display: 'block', cursor: 'pointer' }}>
                      <Image
                        src={require('../../../assets/img/copy-icon.png')}
                        className="mb-3 pos-absolute"
                      />
                      <span className="form-control">
                        {copied ? 'Copied' : 'Copy'} Secret Recover Key
                      </span>
                    </a>
                  </CopyToClipboard>
                </div>
              </div>,
            ],
            buttonClick: this.onSubmitPassword,
            buttonName: 'Continue',
            cancelButtonFlag: false,
            cancelButtonName: 'Cancel',
            cancelIconFlag: false,
            modalName: 'getRecoveryKey',
          });
        } else {
          this.props.incorrectRecoveryCodeAtRedirect(
            passwordForAnotherAccount.value
          );
        }
        console.log("it's working");
      } else {
        this.props.incorrectRecoveryCodeAtRedirect(
          passwordForAnotherAccount.value
        );
      }
    }
    // else {
    //   this.props.incorrectRecoveryCodeAtRedirect();
    // }
  };

  // showUserSecretKey = () => {
  //   this.props.openSignInModal({
  //     title: 'Secret recovery key',

  //     body: [
  //       <div>
  //         <div className="card-c text-center ">
  //           <div className=" view-pad1  clearfix">
  //             <CopyToClipboard text={hexCode} onCopy={onCopy}>
  //               <a
  //                 style={{
  //                   display: 'block',
  //                   cursor: 'pointer',
  //                 }}
  //                 title="Copy Code"
  //               >
  //                 <div className="sub-title d-flex align-items-center justify-content-center text-center">
  //                   Recovery Code {copied ? 'Copied' : 'Copy'}
  //                   <svg
  //                     width="22"
  //                     version="1.1"
  //                     id="Layer_1"
  //                     xmlns="http://www.w3.org/2000/svg"
  //                     xmlnsXlink="http://www.w3.org/1999/xlink"
  //                     x="0px"
  //                     y="0px"
  //                     viewBox="0 0 20 20"
  //                     style={{
  //                       enableBackground: 'new 0 0 20 20',
  //                     }}
  //                     className="ml-3"
  //                     xmlSpace="preserve"
  //                   >
  //                     {/* <style type="text/css">
  //                                 .st0{
  //                                   fill-rule:evenodd;
  //                                   clip-rule:evenodd;
  //                                   fill:#333333;
  //                                 }
  //                                 .st1{
  //                                   clip-path:url(#SVGID_2_);
  //                                   fill-rule:evenodd;
  //                                   clip-rule:evenodd;
  //                                   fill:#FFFFFF;
  //                                 }
  //                                 .st2{
  //                                   fill:#333333;
  //                                 }
  //                                 .st3{
  //                                   fill:#BE0000;
  //                                 }
  //                               </style> */}
  //                     <g>
  //                       <g>
  //                         <path
  //                           className="st3"
  //                           d="M19.2,0H5C4.3,0,4.2,0.1,4.2,0.8c0,0.5,0,1,0,1.5l0,1l-1.3,0c-0.3,0-0.6,0-0.9,0c-0.4,0-0.8,0-1.2,0
  //                                         C0.2,3.4,0,3.5,0,4.2v15.1C0,19.8,0.2,20,0.8,20l14.2,0c0.8,0,0.9-0.1,0.9-0.9l0-2.5l1.2,0c0.7,0,1.4,0,2.1,0
  //                                         c0.7,0,0.8-0.2,0.8-0.8v-15C20,0.1,19.9,0,19.2,0z M14.7,18.9H1.2V4.5h13.5V18.9z M18.8,15.5h-3l0-11.1c0-0.9-0.1-1-1-1H5.3V1.1
  //                                         h13.6V15.5z M4.7,15.4c2.2,0,4.3,0,6.5,0c0.2,0,0.4-0.1,0.5-0.2c0.1-0.1,0.1-0.2,0.1-0.4c0-0.2-0.1-0.5-0.7-0.5
  //                                         c-0.7,0-1.5,0-2.2,0l-1,0l-1,0c-0.3,0-0.7,0-1,0c-0.4,0-0.8,0-1.3,0c-0.4,0-0.7,0.2-0.7,0.5c0,0.2,0.1,0.3,0.2,0.4
  //                                         C4.3,15.3,4.5,15.4,4.7,15.4z M4.7,9.1C4.7,9.1,4.7,9.1,4.7,9.1c1.4,0,2.8,0,4.2,0l2.1,0c0.6,0,0.7-0.3,0.7-0.5
  //                                         c0-0.3-0.1-0.5-0.7-0.5c-0.8,0-1.6,0-2.4,0L7.9,8l-1,0C6.6,8,6.3,8,6,8C5.6,8,5.1,8,4.7,8C4.5,8,4.3,8.1,4.2,8.2
  //                                         C4.1,8.3,4,8.4,4,8.6C4.1,8.7,4.1,9.1,4.7,9.1z"
  //                         />
  //                       </g>
  //                     </g>
  //                   </svg>
  //                 </div>
  //               </a>
  //             </CopyToClipboard>
  //           </div>
  //           <div className="view-pad  border-top back-color ">
  //             <p className="color-red" style={{ wordBreak: 'break-all' }}>
  //               {hexCode}
  //             </p>
  //           </div>
  //         </div>
  //       </div>,
  //     ],
  //     buttonClick: this.onSubmitPassword,

  //     buttonName: 'Submit',
  //     cancelButtonFlag: false,
  //     cancelButtonName: 'Cancel',
  //     cancelIconFlag: false,
  //     modalName: 'getRecoveryKey',
  //   });
  // };

  render() {
    const { dappData } = this.state;
    const { dapps, modalName } = this.props;
    // if (window.location.pathname === '/dappStore') {
    //   this.getRecoveryKey();
    // }
    if (modalName !== '' && this[modalName] !== undefined) {
      this[modalName]();
    }
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    // console.log('from dapp art', dapps.art);
    return (
      // <!--start section -->
      <section className="m-101 " id="dapp-store">
        <div className="container-big">
          <div className="row ">
            <div
              className="col-lg-12 col-md-12 col-sm-12 col-12 aos-item "
              data-aos="fade-top"
            >
              {/* <h1 className="text-center">Top Apps</h1> */}
              {/* <div className="bor-upper ml-5 mx-auto" /> */}
              <div className="row">
                <div className="col col-12 d-flex justify-content-end align-items-center">
                  {mnemonicCode ? (
                    <NavLink to="/register-dapp">
                      <button
                        className="btn  btn-primary mr-4 ml-5"
                        type="button"
                      >
                        Submit DApp
                      </button>
                    </NavLink>
                  ) : null}

                  {/* <NavLink to="">View All</NavLink> */}
                </div>
              </div>
              {/* <div className="row bor-bottom mt-5 mob-mar-none"> */}
              {/* {dapps.all.map(dapp => (
                  <DappCard
                    id={1}
                    key={dapp.url}
                    title={dapp.dappname}
                    description={dapp.dappdetails}
                    src={dapp.dapplogo}
                    rating={dapp.rating}
                  />
                ))} */}
              {/* </div> */}
              {dapps.myDapps.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">My Dapps</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.myDapps.map(dapp => (
                      // <div onClick={this.dapplink}>
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                      // </div>
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.art.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Art</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.art.map(dapp => (
                      // <div onClick={this.dapplink}>
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                      // </div>
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.utilitiesAndProductivity.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Utilities & Productivity</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.utilitiesAndProductivity.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.businessTools.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Business Tools</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.businessTools.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.chat.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Chat</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.chat.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.developerTools.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Developer Tools</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.developerTools.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.documentsAndStorage.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Documents and Storage</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.documentsAndStorage.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.educationAndNews.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Education and News</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.educationAndNews.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.financialServices.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Financial Services</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.financialServices.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.gamesAndDigitalAssets.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Games and Digital Assets</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.gamesAndDigitalAssets.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.healthAndFitness.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Health and Fitness</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.healthAndFitness.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.marketPlaces.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Marketplaces</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.marketPlaces.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.musicPhotoAndVideo.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Music, Photo & Video</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.musicPhotoAndVideo.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.socialImpact.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Social Impact</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.socialImpact.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.socialNetworking.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Social Networking</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.socialNetworking.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {dapps.wallets.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Wallets</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.wallets.map(dapp => (
                      <DappCard
                        id={dapp._id}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null}
              {/* {dapps.all.length > 0 ? (
                <div className="mt-4">
                  <h1 className="text-center">Art</h1>
                  <div className="bor-upper mx-auto" />
                  <div className="text-right mar-53">
                    <NavLink to="">View All</NavLink>
                  </div>
                  <div className="row bor-bottom mt-5 mob-mar-none">
                    {dapps.all.map(dapp => (
                      <DappCard
                        id={1}
                        key={dapp.url}
                        title={dapp.dappname}
                        description={dapp.dappdetails}
                        src={dapp.dapplogo}
                        rating={dapp.rating}
                      />
                    ))}
                  </div>
                </div>
              ) : null} */}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

DappStore.propTypes = {
  dapps: PropTypes.array,
  getDapps: PropTypes.func,
};

const mapStateToProps = ({ dapp, auth, successModal, modal, profile }) => {
  const { dapps } = dapp;
  const { password } = auth;
  const { signInModalFlag, modalName } = modal;
  const { successModalFlag } = successModal;
  const {
    passwordForAnotherAccount,
    disabledButton,
    userSecretRecoveryCode,
  } = profile;
  return {
    dapps,
    password,
    signInModalFlag,
    successModalFlag,
    passwordForAnotherAccount,
    disabledButton,
    userSecretRecoveryCode,
    modalName,
  };
};

const mapDispatchToProps = dispatch => ({
  getDapps: payload => dispatch(actions.getDapps(payload)),
  openSignInModal: payload => dispatch(actions.openSignInModal(payload)),
  storePasswordForAnotherAccount: payload =>
    dispatch(actions.storePasswordForAnotherAccount(payload)),
  clearPassword: () => dispatch(actions.clearPasswordForAnotherAccount()),
  onDisabledButton: payload => dispatch(actions.disabledButton(payload)),
  closeSignInModal: () => dispatch(actions.closeSignInModal()),
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
  userPasswordChanged: payload =>
    dispatch(actions.userPasswordChanged(payload)),
  recoverWallet: payload => dispatch(actions.recoverWallet(payload)),
  recoverWalletSuccess: payload =>
    dispatch(actions.recoverWalletSuccess(payload)),
  incorrectRecoveryCodeAtRedirect: () =>
    dispatch(actions.incorrectRecoveryCodeAtRedirect()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DappStore);
