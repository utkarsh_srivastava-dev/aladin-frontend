import React from 'react';

function AboutUs() {
  return (
    <div>
      {/* <!-- about us section --> */}
      <section className="m-101 pt-3" id="about">
        <div className="container">
          <div className="row ">
            <div
              className="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex aos-item align-items-center"
              data-aos="fade-down"
            >
              <div>
                <h1>ABOUT US</h1>
                <div className="bor-upper" />
                <p>
                  Aladin Network was founded with the goal of changing the way
                  we relate to and use the internet.
                </p>

                <p>
                  The internet is one of the modern era’s greatest invention and
                  it has changed society in ways we previously could not
                  imagine, however, more people are waking up to the ways that
                  the internet has concentrated power in the hands of a few
                  conglomerate corporations, eroding personal privacy, and taken
                  away our rights.
                </p>

                <p>
                  Aladin is the network we have been wishing for. It provides a
                  new way for everyone to use the internet and access the tools
                  and applications they need. It does this while fundamentally
                  ensuring that the internet remains decentralized, fair, and
                  user controlled.
                </p>

                <p>
                  It has taken a series of innovations for a decentralized
                  network such as Aladin to exist, but through combining decades
                  of innovations with a deep understanding of what are the real
                  problems we are facing, Aladin has been able to provide a
                  solution that has solved the core limitations holding us all
                  back.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default AboutUs;
