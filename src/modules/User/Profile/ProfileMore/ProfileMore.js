import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, FormFeedback } from 'reactstrap';
import * as actions from '../../../../actions';
import Button from '../../../../components/InputControls/Button/Button';
import Image from '../../../../components/Image/Image';

class ProfileMore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [
        {
          id: 1,
          name: 'karsh.a.edu',
          id: '1233WDDDXXSDFFF',
          DefaultID: 'name is done by me',
        },
        {
          id: 2,
          name: 'karsh.a.edu',
          id: '1233WDDDXXSDFFF',
          DefaultID: 'name is done by me',
        },
      ],
      storeSelectedId: '',
    };
  }

  onCloseModal = () => {
    const {
      onCloseSignInModal,
      clearStoredPasswrod,
      onClearStoredUserName,
    } = this.props;
    onCloseSignInModal();

    clearStoredPasswrod();
    onClearStoredUserName();
  };

  onCheckUsername = () => {
    const {
      userName,
      formValidatorOnCheckUser,
      checkUserNameAvailaibility,
    } = this.props;
    if (userName.value.length === 0) {
      formValidatorOnCheckUser({});
    }
    if (
      userName.value.length > 0 &&
      /(?<=\s|^)[^.\s]+\.[^.\s]+\.[^.\s]/gm.test(userName.value)
    ) {
      checkUserNameAvailaibility(userName.value);
    }
  };

  onSubmitHandler = () => {
    const {
      userName,
      passwordForAnotherAccount,
      onCreateAnotherAccount,
      onDisabledButton,
    } = this.props;

    if (
      userName.userNameApiResponse &&
      passwordForAnotherAccount.value.length > 0
    ) {
      onDisabledButton(true);
      onCreateAnotherAccount({
        password: passwordForAnotherAccount.value,
        userName: userName.value,
        openSuccessModal: this.openSuccessModal,
      });
    }
  };

  onOpenSearchModal = () => {
    const {
      userName,
      onStoreUserNameOnCheckAvailibility,
      storePasswordForAnotherAccount,
      passwordForAnotherAccount,
      disabledButton,
      usernameApiValidation,
    } = this.props;
    const password = { ...passwordForAnotherAccount };

    this.props.onOpenModal({
      title: 'Search for your username',
      body: [
        <div>
          <div>
            <p>
              Add a username to your Aladin ID so you can interact with other
              people on the decentralized internet.
            </p>
          </div>

          <div className="pos-relative mt-3">
            <Image
              src={require('../../../../assets/img/icon-a.png')}
              className="mb-3 pos-absolute"
            />
            <Input
              type="text"
              name="Username"
              placeholder="Search for Username"
              valid={userName.isTouched ? userName.isValid : false}
              invalid={userName.isTouched ? !userName.isValid : false}
              onBlur={this.onCheckUsername}
              onChange={e => onStoreUserNameOnCheckAvailibility(e.target.value)}
            />
            <FormFeedback
              style={{ textAlign: 'left' }}
              valid={userName.isValid}
              invalid={!userName.isValid}
            >
              {userName.message}
              {/* {password.message} */}
            </FormFeedback>
          </div>
          <div className="pos-relative mt-3">
            <Image
              src={require('../../../../assets/img/lock-img.png')}
              className="mb-3 lock-img pos-absolute"
              alt="yoo"
            />
            <Input
              type="password"
              name="Password"
              placeholder="Enter Password"
              valid={password.isTouched ? password.isValid : false}
              invalid={password.isTouched ? !password.isValid : false}
              onChange={e => storePasswordForAnotherAccount(e.target.value)}
            />
            <FormFeedback
              style={{ textAlign: 'left' }}
              valid={password.isValid}
              invalid={!password.isValid}
            >
              {password.message}
              {/* {password.message} */}
            </FormFeedback>
          </div>
        </div>,
      ],
      buttonName: disabledButton ? 'Loading...' : 'Submit',
      buttonClick: this.onSubmitHandler,
      cancelButton: this.onCloseModal,
      modalName: 'onOpenSearchModal',
      cancelButtonFlag: true,
      cancelButtonName: 'Cancel',
      disabled:
        !(
          userName.isValid &&
          password.isValid &&
          userName.userNameApiResponse
        ) || disabledButton,
    });
  };

  // openAddIdModal = () => {
  //   const {
  //     onOpenModal,
  //     passwordForAnotherAccount,
  //     storePasswordForAnotherAccount,
  //   } = this.props;
  //   const password = { ...passwordForAnotherAccount };
  //   console.log(password.isValid);
  //   onOpenModal({
  //     title: 'Enter Password',
  //     body: [
  //       <div>
  //         <div className="pos-relative mt-3">
  //           <Image
  //             src={require('../../../../assets/img/lock-img.png')}
  //             className="mb-3 lock-img pos-absolute"
  //             alt="yoo"
  //           />
  //           <Input
  //             type="password"
  //             name="Password"
  //             placeholder="Enter Password"
  //             valid={password.isTouched ? password.isValid : false}
  //             invalid={password.isTouched ? !password.isValid : false}
  //             onChange={e => storePasswordForAnotherAccount(e.target.value)}
  //           />
  //           <FormFeedback
  //             style={{ textAlign: 'left' }}
  //             valid={password.isValid}
  //             invalid={!password.isValid}
  //           >
  //             {password.message}
  //             {/* {password.message} */}
  //           </FormFeedback>
  //         </div>
  //       </div>,
  //     ],
  //     buttonName: 'Continue',
  //     cancelButtonFlag: false,
  //     buttonClick: this.onSubmitHandler,
  //     cancelButton: this.onCloseModal,
  //     modalName: 'openAddIdModal',
  //   });
  // };

  // openAvailableNameModal = () => {
  //   const { userName } = this.props;

  // };

  openSuccessModal = () => {
    this.props.onOpenSuccessModal({
      title: 'Success',
      message: 'You have successfully resgistered your user name.',
      modalStatus: 2,
      closeModal: this.props.onCloseSuccessModal,
      redirectUrl: '',
      showIcon: true,
    });
    this.props.onCloseSignInModal();
  };

  onClickHandler = (id, username) => {
    const userData = JSON.parse(localStorage.getItem('userData'));
    console.log(id, username);
    userData.defaultId = id;
    userData.currentUser = username;
    localStorage.setItem('userData', JSON.stringify(userData));
    this.setState({ storeSelectedId: id });
  };

  render() {
    // const { addIdModal, availableNameModal, successModal } = this.state;
    const { modalName } = this.props;
    const { storeSelectedId } = this.state;
    if (modalName !== '' && this[modalName] !== undefined) {
      this[modalName]();
    }
    let address;
    let defaultId;
    if (localStorage.getItem('userData') !== null) {
      ({ address, defaultId } = JSON.parse(localStorage.getItem('userData')));
    }
    console.log(defaultId);
    return (
      <div>
        <section className="page-section">
          <div className="bg-img1  d-flex align-items-center">
            <div className="container padingbt-40  height-pass back-color">
              <div className="row   aos-item" data-aos="fade-down">
                <div className="col-lg-7 col-md-9 col-sm-12 col-xs-12 mx-auto ">
                  <NavLink to="/profile">
                    <Button className="btn btn-primary" type="button">
                      Back
                    </Button>
                  </NavLink>
                  <div id="Storage-more">
                    {address.length > 0
                      ? address.map((item, index) => (
                          <div
                            className="card-d"
                            key={item.address}
                            onClick={() =>
                              this.onClickHandler(item.address, item.username)
                            }
                          >
                            <ul>
                              <li>
                                {' '}
                                <Image
                                  src={require('../../../../assets/img/img-more.png')}
                                />
                              </li>
                              <li>
                                {' '}
                                <p className="font-16 color-black">
                                  {item.username}
                                </p>
                                <br />
                                <span className="font-14 color-grey">
                                  {' '}
                                  ID-{item.address}
                                  <br />
                                  {item.address === defaultId ||
                                  item.address === storeSelectedId ? (
                                    <span>
                                      Default ID
                                      <i
                                        className="fa fa-check color-black ml-1"
                                        aria-hidden="true"
                                      />
                                    </span>
                                  ) : null}
                                  {/* {index === 0
                                    ? (<p>Default ID</p>,
                                      (
                                        <i
                                          className="fa fa-check color-black ml-1"
                                          aria-hidden="true"
                                        />
                                      ))
                                    : null} */}
                                </span>
                              </li>
                            </ul>
                          </div>
                        ))
                      : null}

                    <div id="Storage-pro">
                      <Button
                        className="btn d-block mx-auto mt-4 btn-primary"
                        data-toggle="modal"
                        data-target="#more-profile"
                        type="button"
                        onClick={this.onOpenSearchModal}
                      >
                        Add Another ID
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = ({ profile, modal, auth }) => {
  const { passwordForAnotherAccount, disabledButton } = profile;
  const { modalName } = modal;
  const { userName, usernameApiValidation } = auth;
  return {
    passwordForAnotherAccount,
    modalName,
    userName,
    disabledButton,
    usernameApiValidation,
  };
};

const mapDispatchToProps = dispatch => ({
  onOpenModal: payload => dispatch(actions.openSignInModal(payload)),
  onOpenSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  onCloseSignInModal: () => dispatch(actions.closeSignInModal()),
  onCloseSuccessModal: () => dispatch(actions.closeSuccessModal()),
  storePasswordForAnotherAccount: payload =>
    dispatch(actions.storePasswordForAnotherAccount(payload)),
  clearStoredPasswrod: () => dispatch(actions.clearPasswordForAnotherAccount()),
  onStoreUserNameOnCheckAvailibility: payload =>
    dispatch(actions.storeUserNameOnCheckAvailibility(payload)),
  onClearStoredUserName: () => dispatch(actions.clearStoredUserName()),
  checkUserNameAvailaibility: payload =>
    dispatch(actions.checkUserNameAvailibility(payload)),
  formValidatorOnCheckUser: payload =>
    dispatch(actions.formValidatorOnCheckUser(payload)),
  onCreateAnotherAccount: payload =>
    dispatch(actions.checkUserPassword(payload)),
  onDisabledButton: payload => dispatch(actions.disabledButton(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileMore);
