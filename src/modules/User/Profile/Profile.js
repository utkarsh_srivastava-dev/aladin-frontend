import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import * as actions from '../../../actions';
import Image from '../../../components/Image/Image';
import Button from '../../../components/InputControls/Button/Button';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: false,
      profileName: '',
      profileDescription: '',
      description: false,
    };
  }

  componentWillMount() {
    const { getBalance, getProfileData } = this.props;
    getBalance();
    getProfileData();
  }

  // componentDidMount() {
  //   const { getBalance, getProfileData } = this.props;
  //   getBalance();
  //   getProfileData();
  // }

  uploadProfile = image => {
    const { profileImageChanged } = this.props;
    if (image.length > 0) {
      console.log('TCL: Profile -> image', image[0]);
      profileImageChanged(image[0]);
    }
  };

  editHandler = () => {
    this.setState({ userName: !this.state.userName });
    if (this.state.description) {
      this.setState({ description: false });
    }
  };

  changeHandler = e => {
    this.setState({ profileName: e.target.value });
  };

  onEditDescription = () => {
    this.setState({ description: !this.state.description });
    if (this.state.userName) {
      this.setState({ userName: false });
    }
  };

  changeDescriptionHandler = e => {
    this.setState({ profileDescription: e.target.value });
  };

  submitHandler = () => {
    const { profileName, profileDescription } = this.state;
    const { updateUserProfile, profilePicture } = this.props;
    console.log(profileName, profileDescription);
    if (profileName.length > 0 && profileDescription.length > 0) {
      this.setState({ description: false, userName: false });
      updateUserProfile({
        name: profileName,
        description: profileDescription,
        imageUrl: profilePicture,
      });
    }
  };

  render() {
    const { balance, profilePicture, qrCode, userProfileData } = this.props;
    const { defaultId, currentUser } = JSON.parse(
      localStorage.getItem('userData')
    );
    console.log(
      userProfileData,
      this.state.userName,
      this.state.profileName.length > 0
    );
    return (
      <section className="page-section" id="profile">
        <div className="bg-img1  d-flex align-items-center">
          <div className="container padingbt-40   back-color">
            <div className="row  aos-item" data-aos="fade-down">
              <div className="col-lg-7 col-md-9 col-sm-12 col-xs-12 mx-auto ">
                <div className="row">
                  <div className="col-lg-4 col-md-12 profile-version col-sm-12 col-xs-12 mx-auto ">
                    <div className="profile-pic">
                      <Image
                        src={
                          profilePicture ||
                          require('../../../assets/img/img-profile-dummy.png')
                        }
                        alt=""
                      />
                      <div className="profile-pic-btn">
                        <input
                          type="file"
                          id="file"
                          onChange={e => this.uploadProfile(e.target.files)}
                        />
                        <a href="#" className="btn">
                          <i className="fa fa-camera" aria-hidden="true" />
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-8 col-md-12 col-sm-12 col-xs-12 mx-auto center-profile mt-4">
                    <p className="sub-title" onClick={this.editHandler}>
                      {this.state.userName ? (
                        <input
                          type="text"
                          placeholder="Please enter username"
                          value={this.state.profileName}
                          onChange={this.changeHandler}
                          onClick={e => e.stopPropagation()}
                        />
                      ) : this.state.profileName.length > 0 ? (
                        this.state.profileName
                      ) : userProfileData.name.length > 0 ? (
                        userProfileData.name
                      ) : (
                        'Add username'
                      )}

                      <i
                        className="fa fa-pencil ml-2 color-red"
                        aria-hidden="true"
                      />
                    </p>
                    <p className="sub-title ">{currentUser}</p>
                    <p className="font-16">ID - {defaultId}</p>
                    <p className="font-16" onClick={this.onEditDescription}>
                      {this.state.description ? (
                        <input
                          type="text"
                          placeholder="Please describe something"
                          value={this.state.profileDescription}
                          onChange={this.changeDescriptionHandler}
                          onClick={e => e.stopPropagation()}
                        />
                      ) : this.state.profileDescription.length > 0 ? (
                        this.state.profileDescription
                      ) : userProfileData.description.length > 0 ? (
                        userProfileData.description
                      ) : (
                        'Describe yourself'
                      )}
                      <i
                        className="fa fa-pencil ml-2 color-red"
                        aria-hidden="true"
                      />
                    </p>
                    <Button
                      className="btn mt-3 btn-primary profile-save mr-2"
                      onClick={this.submitHandler}
                    >
                      Save
                    </Button>
                    <NavLink to="/profile-more">
                      <Button className="btn mt-3 btn-primary profile-save">
                        More
                      </Button>
                    </NavLink>
                  </div>
                </div>
                <div className="mt-3 text-cen">
                  <p>
                    <a href="#" data-toggle="modal" data-target="#modalstatus">
                      DApp Approval Status
                    </a>
                  </p>
                  <p className="color-black mt-2">Token Earned 30 ALa</p>
                </div>
                <div id="Storage-pro" className="text-center mt-5">
                  <p className="mt-5 mb-1">Balance</p>
                  <h1 className="mb-1">
                    <b>{balance}</b>
                  </h1>
                  <p className="mb-4">$0.00 USD</p>
                  <Image src={qrCode} className="d-block mx-auto mt-3" />
                  <p className="mt-4">{defaultId}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Profile.propTypes = {
  balance: PropTypes.string,
  getBalance: PropTypes.func,
  profilePicture: PropTypes.string,
  qrCode: PropTypes.string,
};

const mapStateToProps = ({ auth, profile }) => {
  const { balance, qrCode } = auth;
  const { profilePicture, userProfileData } = profile;
  return { balance, profilePicture, qrCode, userProfileData };
};

const mapDispatchToProps = dispatch => ({
  getBalance: payload => dispatch(actions.getBalance(payload)),
  profileImageChanged: payload =>
    dispatch(actions.profileImageChanged(payload)),
  getProfileData: () => dispatch(actions.getProfileData()),
  updateUserProfile: payload => dispatch(actions.updateUserProfile(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
