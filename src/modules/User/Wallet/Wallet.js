import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, FormFeedback } from 'reactstrap';
import Image from '../../../components/Image/Image';

import Button from '../../../components/InputControls/Button/Button';
// import Input from '../../../components/InputControls/Input/Input';
import * as actions from '../../../actions';

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { getBalance } = this.props;
    getBalance();
  }

  test = value => {
    console.log('TCL: Wallet -> test -> value', value);
  };

  closeSuccessModal = () => {
    const { closeSuccessModal } = this.props;
    closeSuccessModal();
  };

  openSuccessModal = () => {
    console.log('TCL: Wallet -> Success -> click');
    const {
      senderWalletAddress,
      recieverWalletAddress,
      storeWalletAmmount,
      storePasswordOnSendTokken,
      openSuccessModal,
      closeModal,
      sendTokkenData,
      balance,
      closeSuccessModal,
      onDisabledButton,
    } = this.props;
    onDisabledButton(true);
    if (
      senderWalletAddress.value.length > 0 &&
      recieverWalletAddress.value.length > 0 &&
      storeWalletAmmount.value.length > 0 &&
      storePasswordOnSendTokken.value.length > 0
    ) {
      sendTokkenData({
        sender: senderWalletAddress.value,
        receiver: recieverWalletAddress.value,
        amount: storeWalletAmmount.value,
        password: storePasswordOnSendTokken.value,
        closeModal: this.closeSuccessModal,
      });
    }
    // openSuccessModal({
    //   title: 'Success',
    //   message: 'Your Token(s) were successfully sent.',
    //   modalStatus: 2,
    //   closeModal: this.closeSuccessModal,
    //   showIcon: true,
    // });
    // closeModal();
  };

  closeAndClear = () => {
    this.props.clearSendTokkenData();
    this.props.closeModal();
  };

  openTokenModal = () => {
    const {
      openModal,
      closeModal,
      senderWalletAddress,
      recieverWalletAddress,
      storeWalletAmmount,
      storePasswordOnSendTokken,
      onStoreAddressOfReciever,
      onStoreAddressOfSender,
      onStoreWalletAmmount,
      onStorePasswordOnSendTokken,
      disabledButton,
    } = this.props;
    console.log(senderWalletAddress.isValid);

    openModal({
      title: 'Send Token',
      body: [
        <div>
          <Input
            type="text"
            name="user"
            className="form-control pl-3"
            placeholder="Enter your Wallet Address"
            valid={senderWalletAddress.isTouched && senderWalletAddress.isValid}
            invalid={
              senderWalletAddress.isTouched && !senderWalletAddress.isValid
            }
            onChange={e => onStoreAddressOfSender(e.target.value)}
          />
          <FormFeedback
            style={{ textAlign: 'left' }}
            valid={senderWalletAddress.isValid}
            invalid={!senderWalletAddress.isValid}
          >
            {senderWalletAddress.message}
            {/* {password.message} */}
          </FormFeedback>
          <Input
            type="text"
            name="user"
            className="form-control mt-3  pl-3"
            placeholder="Enter reciever wallet address"
            valid={
              recieverWalletAddress.isTouched && recieverWalletAddress.isValid
            }
            invalid={
              recieverWalletAddress.isTouched && !recieverWalletAddress.isValid
            }
            onChange={e => onStoreAddressOfReciever(e.target.value)}
          />
          <FormFeedback
            style={{ textAlign: 'left' }}
            valid={recieverWalletAddress.isValid}
            invalid={!recieverWalletAddress.isValid}
          >
            {recieverWalletAddress.message}
            {/* {password.message} */}
          </FormFeedback>
          <Input
            type="text"
            name="user"
            className="form-control mt-3  pl-3"
            placeholder="Enter Token Amount"
            valid={storeWalletAmmount.isTouched && storeWalletAmmount.isValid}
            invalid={
              storeWalletAmmount.isTouched && !storeWalletAmmount.isValid
            }
            onChange={e => onStoreWalletAmmount(e.target.value)}
          />
          <FormFeedback
            style={{ textAlign: 'left' }}
            valid={storeWalletAmmount.isValid}
            invalid={!storeWalletAmmount.isValid}
          >
            {storeWalletAmmount.message}
            {/* {password.message} */}
          </FormFeedback>
          <Input
            type="password"
            name="user"
            className="form-control mt-3  pl-3"
            placeholder="Enter password"
            valid={
              storePasswordOnSendTokken.isTouched &&
              storePasswordOnSendTokken.isValid
            }
            invalid={
              storePasswordOnSendTokken.isTouched &&
              !storePasswordOnSendTokken.isValid
            }
            onChange={e => onStorePasswordOnSendTokken(e.target.value)}
          />
          <FormFeedback
            style={{ textAlign: 'left' }}
            valid={storePasswordOnSendTokken.isValid}
            invalid={!storePasswordOnSendTokken.isValid}
          >
            {storePasswordOnSendTokken.message}
            {/* {password.message} */}
          </FormFeedback>
        </div>,
      ],
      buttonClick: this.openSuccessModal,
      buttonName: disabledButton ? 'Loading...' : 'Send',
      cancelButton: this.closeAndClear,
      cancelButtonFlag: false,
      modalName: 'openTokenModal',
      disabled:
        !(
          senderWalletAddress.isValid &&
          recieverWalletAddress.isValid &&
          storeWalletAmmount.isValid &&
          storePasswordOnSendTokken.isValid
        ) || disabledButton,
    });
  };

  render() {
    const { balance, qrCode, modalName } = this.props;
    if (modalName != '' && this[modalName] != undefined) {
      this[modalName]();
    }
    const { defaultId } = JSON.parse(localStorage.getItem('userData'));

    const { currentUser } = JSON.parse(localStorage.getItem('userData'));
    return (
      <div>
        <section className="page-section" id="view-a">
          <div className="bg-img1  d-flex align-items-center">
            <div className="container padbt-40 text-center  back-color">
              <div className="row  aos-item" data-aos="fade-down">
                <div className="col-lg-7 col-md-9 col-sm-12 col-xs-12 mx-auto ">
                  <div id="Storage-pro">
                    <h4>
                      <b>Wallet</b>
                    </h4>
                    <div className="bor-upper mx-auto" />
                    <p className="mt-5">Balance</p>
                    <h1 className="mb-1">
                      <b>{balance}</b>
                    </h1>
                    <p className="mb-4">$0.00 USD</p>
                    <Image src={qrCode} className="d-block mx-auto mt-3" />
                    <p className="mt-5">{currentUser}</p>

                    <div className="width-460 pt-3">
                      <Button
                        className="btn  mt-4 btn-primary"
                        onClick={this.openModal}
                      >
                        {' '}
                        Receive
                      </Button>
                      <Button
                        className="btn  mt-4 btn-secondary"
                        // data-toggle="modal"
                        // data-target="#modalsentForm"
                        type="button"
                        onClick={this.openTokenModal}
                      >
                        Send
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Wallet.propTypes = {
  openModal: PropTypes.func,
  closeModal: PropTypes.func,
  openSuccessModal: PropTypes.func,
  closeSuccessModal: PropTypes.func,
  getBalance: PropTypes.func,
  balance: PropTypes.string,
};
const mapStateToProps = ({ auth, profile, modal }) => {
  const { balance, qrCode } = auth;
  const {
    senderWalletAddress,
    recieverWalletAddress,
    storeWalletAmmount,
    storePasswordOnSendTokken,
    disabledButton,
  } = profile;
  const { modalName } = modal;
  return {
    balance,
    qrCode,
    senderWalletAddress,
    recieverWalletAddress,
    storeWalletAmmount,
    storePasswordOnSendTokken,
    modalName,
    disabledButton,
  };
};
const mapDisptchToProps = dispatch => ({
  openModal: payload => dispatch(actions.openSignInModal(payload)),
  closeModal: () => dispatch(actions.closeSignInModal()),
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
  getBalance: payload => dispatch(actions.getBalance(payload)),
  onStoreAddressOfSender: payload =>
    dispatch(actions.storeAddressOfSender(payload)),
  onStoreAddressOfReciever: payload =>
    dispatch(actions.storeAddressOfReciever(payload)),
  onStoreWalletAmmount: payload =>
    dispatch(actions.storeWalletAmmount(payload)),
  onStorePasswordOnSendTokken: payload =>
    dispatch(actions.storePasswordOnSendTokken(payload)),
  clearSendTokkenData: () => dispatch(actions.clearSendTokkenData()),
  sendTokkenData: payload => dispatch(actions.sendTokkenData(payload)),
  onDisabledButton: payload => dispatch(actions.disabledButton(payload)),
});
export default connect(mapStateToProps, mapDisptchToProps)(Wallet);
