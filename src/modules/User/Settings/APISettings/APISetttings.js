import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from '../../../../components/InputControls/Button/Button';
import Input from '../../../../components/InputControls/Input/Input';
import * as actions from '../../../../actions';

class APISettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accept: false,
    };
  }

  openResetApiModal = () => {
    const { openSuccessModal } = this.props;
    openSuccessModal({
      title: 'Success',
      message: 'Updated Successfully',
      modalStatus: 2,
      closeModal: this.closeSuccessModal,
      showIcon: true,
    });
  };

  openSaveModal = () => {
    const { openSuccessModal } = this.props;
    openSuccessModal({
      title: 'Success',
      message: 'Updated Successfully',
      modalStatus: 2,
      closeModal: this.closeSuccessModal,
      showIcon: true,
    });
  };

  closeSuccessModal = () => {
    const { closeSuccessModal } = this.props;
    closeSuccessModal();
  };

  render() {
    const { accept } = this.state;
    return (
      <div>
        <section className="page-section" id="view-a">
          <div className="bg-img1  d-flex align-items-center">
            <div className="container padingbt-40 back-color">
              <div className="row  aos-item" data-aos="fade-down">
                <div className="col-lg-7 col-md-9 col-sm-12 col-xs-12 mx-auto ">
                  <NavLink to="/settings">
                    <Button className="btn btn-primary" type="button">
                      Back
                    </Button>
                  </NavLink>
                  <div className="text-left" id="Storage-pro">
                    <h4 className="pada-40">
                      <b>Aladin API Options</b>
                    </h4>
                    <div className="bor-upper" />
                    <div className="border-bot pt-0">
                      <h2 className="font-22">URL for Gaia Hub Connection</h2>
                      <p>https://hub.aladin.org</p>
                    </div>
                    <div className="border-bot">
                      <h2 className="font-22">URL for Gaia Hub Connection</h2>
                      <p>https://hub.aladin.org</p>
                    </div>
                    <div className="border-bot">
                      <h2 className="font-22">URL for Gaia Hub Connection</h2>
                      <p>https://hub.aladin.org</p>
                    </div>
                    <div className="border-bot">
                      <h2 className="font-22">URL for Gaia Hub Connection</h2>
                      <p>https://hub.aladin.org</p>
                    </div>
                    <div className="border-bot">
                      <h2 className="font-22">URL for Gaia Hub Connection</h2>
                      <p>https://hub.aladin.org</p>
                    </div>
                    <label className="container-a text-left" htmlFor="password">
                      <p className="pad-10">
                        <a>Display anonymous analytics</a>
                      </p>
                      <Input
                        type="checkbox"
                        className="password-chekbox"
                        id="password"
                        checked={accept}
                        onChange={() => this.setState({ accept: !accept })}
                      />
                      <span className="checkmark left-align" />
                    </label>
                    <div className="width-460 pt-3">
                      <Button
                        className="btn  mt-4 btn-primary"
                        data-toggle="modal"
                        data-target="#passwordupdate"
                        type="button"
                        onClick={this.openSaveApiModal}
                      >
                        Save
                      </Button>
                      <Button
                        className="btn  mt-4 btn-secondary"
                        data-toggle="modal"
                        data-target="#passwordupdate"
                        type="button"
                        onClick={this.openResetApiModal}
                      >
                        Reset API
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
APISettings.propTypes = {
  openSuccessModal: PropTypes.func,
  closeSuccessModal: PropTypes.func,
};

const mapDisptchToProps = dispatch => ({
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
});

export default connect(
  null,
  mapDisptchToProps
)(APISettings);
