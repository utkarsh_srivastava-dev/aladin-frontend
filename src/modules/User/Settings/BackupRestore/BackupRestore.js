import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
// import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Input, FormFeedback } from 'reactstrap';
import HexCodeBarCode from './HexcodeBarcode';
// import Input from '../../../../components/InputControls/Input/Input';
import Image from '../../../../components/Image/Image';
import Button from '../../../../components/InputControls/Button/Button';
import * as actions from '../../../../actions';

import '../../../Technology/SDK/SDK.css';

class BackupRestore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      magicRecoveryCodeCopied: false,
      secretRecoveryCodeCopied: false,
    };
  }

  componentWillMount() {
    const { getBalance, recoverWalletSuccess, clearPassword } = this.props;
    getBalance();
    recoverWalletSuccess();
  }

  componentWillUnmount() {
    const { clearPassword } = this.props;
    clearPassword();
  }

  copyHandler = () => {
    this.setState({ magicRecoveryCodeCopied: true });
    setTimeout(() => {
      this.setState({ magicRecoveryCodeCopied: false });
    }, 3000);
  };

  copySecretKey = () => {
    this.setState({ secretRecoveryCodeCopied: true });
    setTimeout(() => {
      this.setState({ secretRecoveryCodeCopied: false });
    }, 3000);
  };

  submitHandler = () => {
    const {
      passwordForAnotherAccount,
      recoverWallet,
      onDisabledButton,
    } = this.props;
    if (passwordForAnotherAccount.value.length > 0) {
      onDisabledButton(true);
      const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
      recoverWallet({
        code: mnemonicCode,
        password: passwordForAnotherAccount.value,
      });
    }
  };

  render() {
    const {
      qrCode,
      passwordForAnotherAccount,
      storePasswordForAnotherAccount,
      disabledButton,
      userSecretRecoveryCode,
    } = this.props;
    const { magicRecoveryCodeCopied, secretRecoveryCodeCopied } = this.state;
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));
    console.log('TCL: BackupRestore -> render -> mnemonicCode', mnemonicCode);

    const hexConvertor = dataBuffer => {
      const buffer = new Buffer.from(dataBuffer, 'hex');
      const base = buffer.toString('base64');
      return base;
    };
    const hexCode = hexConvertor(mnemonicCode);
    console.log('TCL: BackupRestore -> render -> hexCode', hexCode);

    return (
      <section className="page-section" id="view-a">
        <div className="bg-img1  d-flex align-items-center">
          <div className="container padbt-40 back-color">
            <div className="row  aos-item" data-aos="fade-down">
              <div className="col-lg-7 col-md-9 col-sm-12 col-xs-12 mx-auto ">
                <NavLink to="/settings">
                  <Button className="btn btn-primary" type="button">
                    Back
                  </Button>
                </NavLink>
                <div id="Storage-pro">
                  <h4 className="pada-40">
                    <b>Magic Recovery Code</b>
                  </h4>

                  <div className="bor-upper" />
                  <p className="m-0">
                    Scan or enter the recovery code with your password to
                    restore your account or sign in on other devices.
                  </p>
                  <HexCodeBarCode
                    hexCode={hexCode}
                    copied={magicRecoveryCodeCopied}
                    qrCode={qrCode}
                    onCopy={this.copyHandler}
                  />

                  <p className="font-24 mb-1 pada-40">Secret Recovery Key</p>
                  <p>
                    Enter your password to view and backup secret recovery key.
                  </p>

                  {userSecretRecoveryCode.length > 0 ? (
                    <HexCodeBarCode
                      qrCode={userSecretRecoveryCode}
                      hexCode={userSecretRecoveryCode}
                      copied={secretRecoveryCodeCopied}
                      onCopy={this.copySecretKey}
                    />
                  ) : (
                    <div className="width-460 pt-3">
                      <div className=" pos-relative">
                        <Image
                          src={require('../../../../assets/img/lock-img.png')}
                          className="mb-3 lock-img pos-absolute"
                        />
                        <Input
                          type="password"
                          name="Password"
                          className="form-control "
                          placeholder="Enter Password"
                          valid={
                            passwordForAnotherAccount.isTouched &&
                            passwordForAnotherAccount.isValid
                          }
                          invalid={
                            passwordForAnotherAccount.isTouched &&
                            !passwordForAnotherAccount.isValid
                          }
                          onChange={e =>
                            storePasswordForAnotherAccount(e.target.value)
                          }
                          value={passwordForAnotherAccount.value}
                        />
                        <FormFeedback
                          style={{ textAlign: 'left' }}
                          valid={passwordForAnotherAccount.isValid}
                          invalid={!passwordForAnotherAccount.isValid}
                        >
                          {passwordForAnotherAccount.message}
                        </FormFeedback>
                      </div>
                      <Button
                        className="btn  mt-4 btn-primary"
                        type="button"
                        disabled={
                          !passwordForAnotherAccount.isValid || disabledButton
                        }
                        onClick={this.submitHandler}
                      >
                        {disabledButton
                          ? 'Loaing...'
                          : 'Display Keychain Phrase'}
                      </Button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
const mapStateToProps = ({ auth, profile }) => {
  const { qrCode } = auth;
  const {
    passwordForAnotherAccount,
    disabledButton,
    userSecretRecoveryCode,
  } = profile;

  return {
    qrCode,
    passwordForAnotherAccount,
    disabledButton,
    userSecretRecoveryCode,
  };
};
const mapDispatchToProps = dispatch => ({
  getBalance: payload => dispatch(actions.getBalance(payload)),
  storePasswordForAnotherAccount: payload =>
    dispatch(actions.storePasswordForAnotherAccount(payload)),
  recoverWallet: payload => dispatch(actions.recoverWallet(payload)),
  onDisabledButton: payload => dispatch(actions.disabledButton(payload)),
  recoverWalletSuccess: payload =>
    dispatch(actions.recoverWalletSuccess(payload)),
  clearPassword: () => dispatch(actions.clearPasswordForAnotherAccount()),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BackupRestore);
