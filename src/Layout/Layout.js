import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Input, FormFeedback } from 'reactstrap';
// import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer.js';
import DynamicModal from '../components/Modals/DynamicModal/DynamicModal';
import SuccessModal from '../components/Modals/SuccessModal/SuccessModal';
import NavbarWithoutLogin from '../components/Navbar/NavbarWithoutLogin';
import NavbarWithLogin from '../components/Navbar/NavbarWithLogin';
import * as actions from '../actions';
import Image from '../components/Image/Image';

class Layout extends Component {
  componentDidMount() {
    // const url = 'http://localhost:3000/id=1234';

    // console.log(
    //   'TCL: Layout -> componentDidMount -> window.location.href',
    //   window.location.href
    // );
    // if (url === window.location.href) {
    //   console.log('working url');
    //   const { password, userPasswordChanged, closeSignInModal } = this.props;
    //   this.props.openSignInModal({
    //     title: 'Enter password',
    //     backButton: [
    //       <p
    //         className="color-red hover-icon mb-0 text-left back"
    //         onClick={this.openLoginModal}
    //         style={{ cursor: 'pointer' }}
    //       >
    //         <i className="fa fa-long-arrow-left back" aria-hidden="true" /> Back
    //       </p>,
    //     ],
    //     body: [
    //       <div>
    //         <div className="pos-relative mt-3">
    //           <Image
    //             src={require('../assets/img/lock-img.png')}
    //             className="mb-3 lock-img pos-absolute"
    //             alt="yoo"
    //           />
    //           <Input
    //             type="password"
    //             name="Password"
    //             placeholder="Enter Password"
    //             valid={password.isTouched ? password.isValid : false}
    //             invalid={password.isTouched ? !password.isValid : false}
    //             value={password.value}
    //             onChange={e => userPasswordChanged(e.target.value)}
    //           />
    //           <FormFeedback
    //             style={{ textAlign: 'left' }}
    //             valid={password.isValid}
    //             invalid={!password.isValid}
    //           >
    //             {password.message}
    //           </FormFeedback>
    //         </div>
    //       </div>,
    //     ],
    //     buttonClick: this.onOpenSuccessModal,
    //     cancelButton: () => {
    //       closeSignInModal();
    //       userPasswordChanged(null);
    //     },
    //     buttonName: 'Sign in',
    //     cancelButtonFlag: true,
    //     cancelButtonName: 'Cancel',
    //     modalName: 'emailData',
    //   });
    // }
    window.scrollTo(0, 0);
    const userStoredData = localStorage.getItem('mnemonicCode');
    // if (!userStoredData) {
    //   this.props.history.push('/');
    // }
  }

  render() {
    const { successModalFlag, signInModalFlag, children } = this.props;
    // console.log(signInModalFlag, successModalFlag);
    const mnemonicCode = JSON.parse(localStorage.getItem('mnemonicCode'));

    return (
      <div>
        {/* <Navbar /> */}
        {mnemonicCode ? <NavbarWithLogin /> : <NavbarWithoutLogin />}

        {signInModalFlag ? <DynamicModal /> : null}
        {successModalFlag ? <SuccessModal /> : null}
        <div>{children}</div>
        <Footer />
      </div>
    );
  }
}

Layout.propTypes = {
  successModalFlag: PropTypes.bool,
  signInModalFlag: PropTypes.bool,
  children: PropTypes.object,
};

const mapStateToProps = ({ auth, successModal, modal }) => {
  const { password } = auth;
  const { signInModalFlag } = modal;
  const { successModalFlag } = successModal;

  return {
    password,
    signInModalFlag,
    successModalFlag,
  };
};
const mapDispatchToProps = dispatch => ({
  openSignInModal: payload => dispatch(actions.openSignInModal(payload)),
  closeSignInModal: () => dispatch(actions.closeSignInModal()),
  openSuccessModal: payload => dispatch(actions.openSuccessModal(payload)),
  closeSuccessModal: () => dispatch(actions.closeSuccessModal()),
  userPasswordChanged: payload =>
    dispatch(actions.userPasswordChanged(payload)),
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Layout)
);
